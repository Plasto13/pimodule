<?php

return [
    'pimodule.equipment' => [
        'index' => 'pimodule::equipment.list resource',
        'create' => 'pimodule::equipment.create resource',
        'edit' => 'pimodule::equipment.edit resource',
        'destroy' => 'pimodule::equipment.destroy resource',
    ],
    'pimodule.inspections' => [
        'index' => 'pimodule::inspections.list resource',
        'create' => 'pimodule::inspections.create resource',
        'edit' => 'pimodule::inspections.edit resource',
        'destroy' => 'pimodule::inspections.destroy resource',
    ],
    'pimodule.equipmentinspections' => [
        'index' => 'pimodule::equipmentinspections.list resource',
        'create' => 'pimodule::equipmentinspections.create resource',
        'edit' => 'pimodule::equipmentinspections.edit resource',
        'destroy' => 'pimodule::equipmentinspections.destroy resource',
    ],
    'pimodule.records' => [
        'index' => 'pimodule::records.list resource',
        'create' => 'pimodule::records.create resource',
        'edit' => 'pimodule::records.edit resource',
        'destroy' => 'pimodule::records.destroy resource',
    ],
   
    'pimodule.recordstatuses' => [
        'index' => 'pimodule::recordstatuses.list resource',
        'create' => 'pimodule::recordstatuses.create resource',
        'edit' => 'pimodule::recordstatuses.edit resource',
        'destroy' => 'pimodule::recordstatuses.destroy resource',
    ],
    'pimodule.settings' => [
        'index' => 'pimodule::settings.list resource',
        'create' => 'pimodule::settings.create resource',
        'edit' => 'pimodule::settings.edit resource',
        'destroy' => 'pimodule::settings.destroy resource',
    ],
    'pimodule.equipment' => [
        'index' => 'pimodule::equipment.list resource',
        'create' => 'pimodule::equipment.create resource',
        'edit' => 'pimodule::equipment.edit resource',
        'destroy' => 'pimodule::equipment.destroy resource',
    ],
    'pimodule.weekendplans' => [
        'index' => 'pimodule::weekendplans.list resource',
        'create' => 'pimodule::weekendplans.create resource',
        'edit' => 'pimodule::weekendplans.edit resource',
        'destroy' => 'pimodule::weekendplans.destroy resource',
    ],
    'pimodule.planrepairs' => [
        'index' => 'pimodule::planrepairs.list resource',
        'create' => 'pimodule::planrepairs.create resource',
        'edit' => 'pimodule::planrepairs.edit resource',
        'destroy' => 'pimodule::planrepairs.destroy resource',
    ],
    'pimodule.repairlogs' => [
        'index' => 'pimodule::repairlogs.list resource',
        'create' => 'pimodule::repairlogs.create resource',
        'edit' => 'pimodule::repairlogs.edit resource',
        'destroy' => 'pimodule::repairlogs.destroy resource',
    ],
    'pimodule.planrepairrecords' => [
        'index' => 'pimodule::planrepairrecords.list resource',
        'create' => 'pimodule::planrepairrecords.create resource',
        'edit' => 'pimodule::planrepairrecords.edit resource',
        'destroy' => 'pimodule::planrepairrecords.destroy resource',
    ],
    'pimodule.patrols' => [
        'index' => 'pimodule::patrols.list resource',
        'create' => 'pimodule::patrols.create resource',
        'edit' => 'pimodule::patrols.edit resource',
        'destroy' => 'pimodule::patrols.destroy resource',
    ],
// append














];
