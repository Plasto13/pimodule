<?php

namespace Modules\Pimodule\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Core\Traits\CanPublishConfiguration;
use Modules\Core\Events\BuildingSidebar;
use Modules\Core\Events\LoadingBackendTranslations;
use Modules\Pimodule\Events\Handlers\RegisterPimoduleSidebar;
use Modules\Media\Image\ThumbnailManager;

class PimoduleServiceProvider extends ServiceProvider
{
    use CanPublishConfiguration;
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerBindings();
        $this->app['events']->listen(BuildingSidebar::class, RegisterPimoduleSidebar::class);

        $this->app['events']->listen(LoadingBackendTranslations::class, function (LoadingBackendTranslations $event) {
            $event->load('inspections', array_dot(trans('pimodule::inspections')));
            $event->load('equipmentinspections', array_dot(trans('pimodule::equipmentinspections')));
            $event->load('records', array_dot(trans('pimodule::records')));
            
            $event->load('recordstatuses', array_dot(trans('pimodule::recordstatuses')));
            $event->load('settings', array_dot(trans('pimodule::settings')));
            $event->load('equipment', array_dot(trans('pimodule::equipment')));
            $event->load('weekendplans', array_dot(trans('pimodule::weekendplans')));
            $event->load('planrepairs', array_dot(trans('pimodule::planrepairs')));
            $event->load('repairlogs', array_dot(trans('pimodule::repairlogs')));
            $event->load('planrepairrecords', array_dot(trans('pimodule::planrepairrecords')));
            $event->load('patrols', array_dot(trans('pimodule::patrols')));
            // append translations














        });
    }

    public function boot()
    {
        $this->publishConfig('pimodule', 'permissions');

        $this->app[ThumbnailManager::class]->registerThumbnail('miniInspectionThumb', [
            'resize' => [
                'width' => 100,
                'height' => null,
                'callback' => function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                },
            ],
        ]);

        $this->loadMigrationsFrom(__DIR__ . '/../Database/Migrations');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

    private function registerBindings()
    {
        $this->app->bind(
            'Modules\Pimodule\Repositories\InspectionRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentInspectionRepository(new \Modules\Pimodule\Entities\Inspection());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CacheInspectionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\EquipmentInspectionRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentEquipmentInspectionRepository(new \Modules\Pimodule\Entities\EquipmentInspection());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CacheEquipmentInspectionDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\RecordRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentRecordRepository(new \Modules\Pimodule\Entities\Record());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CacheRecordDecorator($repository);
            }
        );
        
        $this->app->bind(
            'Modules\Pimodule\Repositories\RecordStatusRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentRecordStatusRepository(new \Modules\Pimodule\Entities\RecordStatus());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CacheRecordStatusDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\SettingsRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentSettingsRepository(new \Modules\Pimodule\Entities\Settings());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CacheSettingsDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\EquipmentRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentEquipmentRepository(new \Modules\Pimodule\Entities\Equipment());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CacheEquipmentDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\WeekendPlanRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentWeekendPlanRepository(new \Modules\Pimodule\Entities\WeekendPlan());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CacheWeekendPlanDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\PlanRepairRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentPlanRepairRepository(new \Modules\Pimodule\Entities\PlanRepair());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CachePlanRepairDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\RepairLogRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentRepairLogRepository(new \Modules\Pimodule\Entities\RepairLog());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CacheRepairLogDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\PlanRepairRecordRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentPlanRepairRecordRepository(new \Modules\Pimodule\Entities\PlanRepairRecord());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CachePlanRepairRecordDecorator($repository);
            }
        );
        $this->app->bind(
            'Modules\Pimodule\Repositories\PatrolRepository',
            function () {
                $repository = new \Modules\Pimodule\Repositories\Eloquent\EloquentPatrolRepository(new \Modules\Pimodule\Entities\Patrol());

                if (! config('app.cache')) {
                    return $repository;
                }

                return new \Modules\Pimodule\Repositories\Cache\CachePatrolDecorator($repository);
            }
        );
// add bindings














    }
}
