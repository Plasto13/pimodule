@php
$id = $equipmentinspection->record()->max('id');
$record = \Modules\Pimodule\Entities\Record::find($id);
@endphp

<div class="btn-group btn-xs" style="width: 170px">

    @if(!isset($record->plan_repair) || ($record && isset($record->planRepair)))
    <button parameter="{{ route('guest.pimodule.record.create', $equipmentinspection->id) }}" class="btn btn-default btn-flat" title="{{trans('pimodule::records.create resource')}}"><i class="fa fa-pencil"></i></button>
    @endif
           
</div>



