@extends('layouts.master')

@section('content-header')
    <h1>
        {{ $planrepair->title }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.pimodule.equipmentinspection.index') }}">{{ trans('pimodule::equipmentinspections.title.equipmentinspections') }}</a></li>
        <li class="active">{{ trans('pimodule::planrepairs.title.planrepairs') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">

                <ul class="timeline">

                    
                    @foreach($logs as $log)
                    @if($loop->first)
                    <!-- timeline time label -->
                    <li class="time-label">
                        <span class="{{ $log->ended ? 'bg-green' : 'bg-blue' }}">
                            {{ $log->ended ? $log->created_at->format('d.m.Y H:m') : \Carbon\Carbon::now()->format('d.m.Y') }}
                        </span>
                    </li>
                    <!-- /.timeline-label -->
         
                    @endif
                    <!-- timeline item -->
                    <li>
                        <!-- timeline icon -->
                        <i class="fa  {{ $log->ended ? 'fa-check bg-green' : 'fa-wrench bg-blue' }}"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-clock-o"></i> {{ $log->created_at->format('d.m.Y G:i') }}</span>

                            <h3 class="timeline-header"><a href="#">{{ $log->title }}</a> ...</h3>

                            <div class="timeline-body">
                                {{ $log->description }}
                            </div>

                            <div class="timeline-footer">
                                @foreach($log->users as $user)
                                    <span class="label bg-light-blue">{{ \Modules\User\Entities\Sentinel\User::find($user)->present()->fullname }}</span>
                                @endforeach
                            </div>
                        </div>
                    </li>
                    <!-- END timeline item -->
                    @endforeach

                    <li class="time-label">
                        <span class="bg-green">
                            {{ $planrepair->created_at->format('d.m.Y') }}
                        </span>
                        <span class="bg-blue">
                            {{ $planrepair->repair_date->format('d.m.Y') }}
                        </span>
                    </li>
                </ul>
               

        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('pimodule::planrepairs.title.create planrepair') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [

                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
@endpush
