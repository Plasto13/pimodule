@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{{asset('themes/adminlte/vendor/admin-lte/plugins/datepicker/datepicker3.css')}}">
<link rel="stylesheet" href="{!! Module::asset('base:css/select2.css') !!}" />
<link rel="stylesheet" href="{!! Module::asset('base:css/select2-bootstrap-dick.css') !!}"/>
@endsection

@push('css-stack')
    
    <style type="text/css">

    table input {
             max-width: 100px;
    }

    .m-b-0 {
    margin-bottom: 0px;
    }

    .base-filter label {
      color: #868686 ;
      font-weight: 600;
      text-transform: uppercase;
    }

    .navbar-filters {
      min-height: 25px !important;
      border-radius: 0 !important;
      margin-bottom: 10px !important;
      margin-left: -10px !important;
      margin-right: -10px!important ;
      margin-top: -11px !important;
      background: #f9f9f9 ;
      border-color: #f4f4f4 ;
    }

    .navbar-filters .navbar-collapse {
        padding: 0 !important;
    }

    .navbar-filters .navbar-toggle {
      padding: 10px 15px !important;
      border-radius: 0 !important;
    }

    .navbar-filters .navbar-brand {
      height: 25px !important;
      padding: 5px 15px !important;
      font-size: 14px !important;
      text-transform: uppercase !important;
    }
    @media (min-width: 768px) {
      .navbar-filters .navbar-nav>li>a {
          padding-top: 5px !important;
          padding-bottom: 5px !important;
      }
    }

    @media (max-width: 768px) {
      .navbar-filters .navbar-nav {
        margin: 0 !important;
      }
    }

    .select2-container {
        display: inline-block !important;
      }
      .select2-drop-active {
        border:none !important;
      }
      .select2-container .select2-choices .select2-search-field input, .select2-container .select2-choice, .select2-container .select2-choices {
        border: none !important;
      }
      .select2-container-active .select2-choice {
        border: none !important;
        box-shadow: none !important;
      }
    </style>
@endpush

@section('content-header')
    <h1>
        {{ trans('pimodule::planrepairs.title.planrepairs') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('guest.pimodule.equipmentInspection.index') }}">{{ trans('pimodule::equipmentinspections.title.equipmentinspections') }}</a></li>
        <li class="active">{{ trans('pimodule::planrepairs.title.planrepairs') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <!-- dorobit menu -->
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  @if (isset($base['filters']))
                    @include('base::inc.filter')
                  @endif
                    <div class="table-responsive">
                       {!! $html->table(['class'=>'table table-bordered table-hover table-striped dataTable'],true) !!}
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
    </dl>
@stop

@push('js-stack')
{!! $html->scripts() !!}
<script src="{!! Module::asset('base:js/select2/select2.js') !!}"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                   
                ]
            });
        });
    </script>    
@endpush
