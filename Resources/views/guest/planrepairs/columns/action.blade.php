@php

@endphp
<div class="btn-group btn-xs" style="width: 170px">
<button class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-record-confirmation-{{$planrepair->id}}" title="{{trans('pimodule::records.create resource')}}"><i class="fa fa-pencil"></i></button>       
<a href="{{ route('guest.pimodule.planrepair.timeline',$planrepair->id) }}" class="btn btn-default btn-flat"  title="{{trans('pimodule::planrepairs.button.timeline')}}"><i class="fa fa-clock-o"></i></a>       
</div>

<div class="modal fade modal-success" id="modal-record-confirmation-{{$planrepair->id}}" tabindex="-1" role="dialog" aria-labelledby="create-confirmation-title" aria-hidden="true">
{!! Form::open(['route' => ['guest.pimodule.repairlog.store',$planrepair->id], 'method' => 'post']) !!}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="create-confirmation-title">{{ trans('core::core.modal.title') }}</h4>
            </div>
            <div class="modal-body">

            <div class="row"> 
        		
        			<div class="col-xs-12 center">
        				{!! Form::label('title', trans('pimodule::records.form.status')) !!}
        			</div>
                	<div class="col-xs-12">
                    	{!! Form::text('title',null,['class' => 'form-control']) !!}
                    </div>
                    <div class="col-xs-12 center">
        				{!! Form::label('description', trans('pimodule::records.form.description') ) !!}
        			</div>
                	<div class="col-xs-12">
                       {!! Form::textarea('description',null,['class' => 'form-control']) !!}
                    </div>
                	<div class="col-xs-12">
                       {!! Form::normalSelect('users', trans('pimodule::records.form.inspector'), $errors, $users, null, ['required', 'multiple'=>'multiple']) !!}
                    </div>
                    <div class="col-xs-12 center">
        				{!! Form::label('ended', trans('pimodule::records.form.ended') ) !!}
        			</div>
                    <div class="col-xs-12">
                    	{!! Form::checkbox('ended') !!}
                    </div>
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
                
                <button type="submit" class="btn btn-outline btn-flat">{{ trans('pimodule::records.button.create') }}</button>
               
            </div>
        </div>
    </div>
     {!! Form::close() !!}
</div>

