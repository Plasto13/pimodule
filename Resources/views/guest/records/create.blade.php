@extends('layouts.master')

@section('styles')
<link rel="stylesheet" href="{!! Module::asset('base:css/dropzone/dropzone.css') !!}" />
<link rel="stylesheet" href="{!! Module::asset('base:css/select2.css') !!}" />
<link rel="stylesheet" href="{!! Module::asset('base:css/select2-bootstrap-dick.css') !!}"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.28/sweetalert2.min.css">
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.26.28/sweetalert2.min.js"></script>
@endsection

@push('css-stack')
<style type="text/css">
    .dropzone {
        border:2px dashed #999999;
        border-radius: 10px;
    }
    .dropzone .dz-default.dz-message {
        height: 171px;
        background-size: 132px 132px;
        margin-top: -101.5px;
        background-position-x:center;

    }
    .dropzone .dz-default.dz-message span {
        display: block;
        margin-top: 145px;
        font-size: 20px;
        text-align: center;
    }
</style>
@endpush


@section('content-header')
    <h1>
        {{ trans('pimodule::records.title.create record') }}
    </h1>
    <ol class="breadcrumb">
      {{--   <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.pimodule.record.index') }}">{{ trans('pimodule::records.title.records') }}</a></li>
        <li class="active">{{ trans('pimodule::records.title.create record') }}</li> --}}
    </ol>
@stop

@section('content')

    {!! Form::open(['route' => ['api.pimodule.equipmentinspections.record.store', $equipmentinspection->id], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                   <div class="row">
                        <div class="col-xs-3">
                            {!! Form::normalSelect('record_status_id', trans('pimodule::records.form.status'),$errors, $recordStatus, null, ['required', 'placeholder' => trans('pimodule::records.form.select status')]) !!}
                        </div>
                        <div class="col-xs-3">
                            {!! Form::normalSelect('user_id', trans('pimodule::records.form.inspector'),$errors, $users, null, ['required', 'placeholder' => trans('pimodule::records.form.select inspector')]) !!}
                        </div>                        
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <div class="dropzone" id="fileupload">
                            
                            <div class="fallback">
                                {{-- <input name="file" type="files" multiple accept="image/jpeg, image/png, image/jpg" /> --}}
                            </div>
                        </div>
                        <div id="addFiles"></div>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            {!! Form::normalTextarea('description', trans('pimodule::records.form.description'),$errors) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-3">
                            {!! Form::normalCheckbox('plan_repair', trans('pimodule::records.form.plan_repair'),$errors) !!}
                        </div>
                   </div>

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" onclick="window.close()"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
{!! Form::close() !!}

@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
<script src="{!! Module::asset('dashboard:vendor/jquery-ui/jquery-ui.min.js') !!}"></script>
<script src="{!! Module::asset('base:js/select2/select2.js') !!}"></script>
<script src="{!! Module::asset('base:js/dropzone/dropzone.js') !!}"></script>
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.pimodule.record.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('.select2').select2()
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });

function sendOpener(result) {
  if (top.opener && !top.opener.closed) {
    try {
    window.opener.HandlePopupResult(); 
    }
    catch(e) {
    }
    window.close();
  }
}

//submiting form
        $('form').submit(function(e) {
            e.preventDefault();
//ckeditor fix
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

            var data = $('form').serialize();
            
             $.ajax(
                {
                url: "{{ route('api.pimodule.equipmentinspections.record.store', $equipmentinspection->id) }}",
                type: 'POST',
                data : data,

                success: function(result){
                    console.log(result);
                     swal({
                      title: result.title,
                      text: result.text,
                      type: result.type,
                      timer: 3000,
                      showConfirmButton: true,
                      onClose: () => {
                        self.close()
                      }
                    }).done();
                },
                error: function(response){
                    console.log(response);
                     swal({
                      title: result.title,
                      text: result.text,
                      type: result.type,
                      showConfirmButton: true,
                    }).done();
                }
            });
        });

    </script>
    <script type="text/javascript">
  Dropzone.options.fileupload = {
    accept: function (file, done) {
      if (file.type != "application/vnd.ms-excel" && file.type != "image/jpeg, image/png, image/jpg") {
        done("Error! Files of this type are not accepted");
      } else {
        done();
      }
    }
  }

Dropzone.options.fileupload = {
  acceptedFiles: "image/jpeg, image/png, image/jpg",
  url:'{{route('api.guest.media.store-dropzone')}}'
}

if (typeof Dropzone != 'undefined') {
  Dropzone.autoDiscover = false;
}

;
(function ($, window, undefined) {
  "use strict";

  $(document).ready(function () {
    // Dropzone
    if (typeof Dropzone != 'undefined') {
      if ($("#fileupload").length) {
        var dz = new Dropzone("#fileupload"),
          dze_info = $("#dze_info"),
          status = {
            uploaded: 0,
            errors: 0
          };
        var $f = $('<tr><td class="name"></td><td class="size"></td><td class="type"></td><td class="status"></td></tr>');
        dz.on("success", function (file, responseText) {

            var _$f = $f.clone();
// add hiden input for store
        $("#addFiles").append($('<input type="hidden" ' +
                                  'name="medias_multi[record_result][files][]" ' +
                                  'value="' + responseText.id + '">'));

            _$f.addClass('success');

            _$f.find('.name').html(file.name);
            if (file.size < 1024) {
              _$f.find('.size').html(parseInt(file.size) + ' KB');
            } else {
              _$f.find('.size').html(parseInt(file.size / 1024, 10) + ' KB');
            }
            _$f.find('.type').html(file.type);
            _$f.find('.status').html('Uploaded <i class="entypo-check"></i>');

            dze_info.find('tbody').append(_$f);

            status.uploaded++;

            dze_info.find('tfoot td').html('<span class="label label-success">' + status.uploaded + ' uploaded</span> <span class="label label-danger">' + status.errors + ' not uploaded</span>');

             const toast = swal.mixin({
              toast: true,
              position: 'top-center',
              showConfirmButton: false,
              timer: 3000
            });

            toast({
              type: 'success',
              title: 'Your File Uploaded Successfully!!'
            });

          })
          .on('error', function (file) {
            var _$f = $f.clone();

            dze_info.removeClass('hidden');

            _$f.addClass('danger');

            _$f.find('.name').html(file.name);
            _$f.find('.size').html(parseInt(file.size / 1024, 10) + ' KB');
            _$f.find('.type').html(file.type);
            _$f.find('.status').html('Uploaded <i class="entypo-cancel"></i>');

            dze_info.find('tbody').append(_$f);

            status.errors++;

            dze_info.find('tfoot td').html('<span class="label label-success">' + status.uploaded + ' uploaded</span> <span class="label label-danger">' + status.errors + ' not uploaded</span>');

            const toast = swal.mixin({
              toast: true,
              position: 'top-center',
              showConfirmButton: false,
              timer: 3000
            });

            toast({
              type: 'error',
              title: 'Your File Uploaded Not Successfully!!'
            });

          });
      }
    }
  });
})(jQuery, window); 

</script>
@endpush
