@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('pimodule::recordstatuses.title.recordstatuses') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.pimodule.equipmentinspection.index') }}">{{ trans('pimodule::equipmentinspections.title.equipmentinspections') }}</a></li>
        <li class="active">{{ trans('pimodule::recordstatuses.title.recordstatuses') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.pimodule.recordstatus.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('pimodule::recordstatuses.button.create recordstatus') }}
                    </a>
                    <a href="{{ route('admin.pimodule.planrepair.index') }}" title="{{ trans('pimodule::planrepairs.button.planrepairs') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <a href="{{ route('admin.pimodule.record.index') }}" title="{{ trans('pimodule::records.button.records') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-circle"></i>
                    </a>
                    <a href="{{ route('admin.pimodule.equipment.index') }}" title="{{ trans('base::equipment.button.equipments') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-cube"></i>
                    </a>
                    <a href="{{ route('admin.pimodule.inspection.index') }}" title="{{ trans('pimodule::inspections.button.inspections') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-flag"></i>
                    </a>
                    <a href="{{ route('admin.pimodule.settings.index') }}" title="{{ trans('pimodule::settings.button.equipment') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-gear"></i>
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('pimodule::recordstatuses.table.title') }}</th>
                                <th>{{ trans('pimodule::recordstatuses.table.description') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($recordstatuses)): ?>
                            <?php foreach ($recordstatuses as $recordstatus): ?>
                            <tr>
                                <td>
                                     <a href="{{ route('admin.pimodule.recordstatus.edit', [$recordstatus->id]) }}">
                                        {{ $recordstatus->title }}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.pimodule.recordstatus.edit', [$recordstatus->id]) }}">
                                        {!! $recordstatus->description !!}
                                    </a>
                                </td>
                                <td>
                                    <a href="{{ route('admin.pimodule.recordstatus.edit', [$recordstatus->id]) }}">
                                        {{ $recordstatus->created_at }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.pimodule.recordstatus.edit', [$recordstatus->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.pimodule.recordstatus.destroy', [$recordstatus->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('pimodule::recordstatuses.table.title') }}</th>
                                <th>{{ trans('pimodule::recordstatuses.table.description') }}</th>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('pimodule::recordstatuses.title.create recordstatus') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.pimodule.recordstatus.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    <script type="text/javascript">
        $(function () {
            $('.data-table').dataTable({
                "paginate": true,
                "lengthChange": true,
                "filter": true,
                "sort": true,
                "info": true,
                "autoWidth": true,
                "order": [[ 0, "desc" ]],
                "language": {
                    "url": '<?php echo Module::asset("core:js/vendor/datatables/{$locale}.json") ?>'
                }
            });
        });
    </script>
@endpush
