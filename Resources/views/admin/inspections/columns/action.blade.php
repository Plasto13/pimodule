<div class="btn-group btn-xs" style="width: 170px">

    <a href="{{ route('admin.pimodule.inspection.edit',[$id]) }}" class="btn btn-default btn-flat" title="{{trans('pimodule::inspection.title.edit item')}}"><i class="fa fa-pencil"></i></a>
    <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.pimodule.inspection.destroy',[$id]) }}" title="{{trans('pimodule::inspection.destroy resource')}}"><i class="fa fa-trash"></i></button>
           
</div>
