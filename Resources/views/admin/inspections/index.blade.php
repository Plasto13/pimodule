@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('pimodule::inspections.title.inspections') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.pimodule.equipmentinspection.index') }}">{{ trans('pimodule::equipmentinspections.title.equipmentinspections') }}</a></li>
        <li class="active">{{ trans('pimodule::inspections.title.inspections') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.pimodule.inspection.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('pimodule::inspections.button.create inspection') }}
                    </a>
                    <button data-toggle="modal" data-target="#ImportModal" class="btn btn-primary btn-flat btn-info" title="{{ trans('pimodule::inspections.button.import items') }}" style="padding: 4px 10px;">
                    <i class="fa fa-upload"> {{ trans('pimodule::inspections.button.import items') }}</i></button>
                    <a href="{{ route('admin.pimodule.planrepair.index') }}" title="{{ trans('pimodule::planrepairs.button.planrepairs') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-wrench"></i>
                    </a>
                    <a href="{{ route('admin.pimodule.record.index') }}" title="{{ trans('pimodule::records.button.records') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-circle"></i>
                    </a>
                    <a href="{{ route('admin.pimodule.equipment.index') }}" title="{{ trans('base::equipment.button.equipments') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-cube"></i>
                    </a>
                    <a href="{{ route('admin.pimodule.recordstatus.index') }}" title="{{ trans('pimodule::recordstatuses.button.recordstatuses') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-list"></i>
                    </a>
                    <a href="{{ route('admin.pimodule.settings.index') }}" title="{{ trans('pimodule::settings.button.equipment') }}" class="btn btn-default btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-gear"></i>
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        {!! $html->table(['class'=>'table table-bordered table-hover dataTable'],true) !!}
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')

    <div tabindex="-1" role="dialog" id="ImportModal" class="modal fade" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Import xls file</h4>
                </div>
                    {!! Form::open(['route' => 'admin.pimodule.inspection.import','method'=>'POST','files'=>true]) !!}
                <div class="modal-body">
                    <div class="form-group ">
                        {!! Form::label('import',trans('pimodule::inspections.form.import label')) !!}
                        {!! Form::file('import',null,['class'=>'form-control']) !!}
                    </div>
                </div>
                <div class="modal-footer">
                    {!! Form::submit(trans('pimodule::inspections.form.import'),['class'=>'btn btn-block btn-primary']) !!}
                </div>
                    {!! Form::close() !!}
                </div>
                <!--Modal-->                
            </div>
        </div>
    </div>

@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('pimodule::inspections.title.create inspection') }}</dd>
    </dl>
@stop

@push('js-stack')
{!! $html->scripts() !!}
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.pimodule.inspection.create') ?>" }
                ]
            });
        });
    </script>
    <?php $locale = locale(); ?>
    
@endpush
