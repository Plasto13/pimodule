@php
$id = $equipmentinspection->record()->max('id');
$record = \Modules\Pimodule\Entities\Record::find($id);
@endphp
<div class="btn-group btn-xs" style="width: 170px">
    @if(!isset($record->plan_repair) || ($record && isset($record->planRepair)))
    <button class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-record-confirmation-{{$equipmentinspection->id}}" title="{{trans('pimodule::records.create resource')}}"><i class="fa fa-pencil"></i></button>
    @endif
    @if(isset($record->plan_repair) && !isset($record->planRepair))
    <button class="btn btn-default btn-flat" data-toggle="modal" data-target="#modal-plan-repair-{{$equipmentinspection->id}}" title="{{trans('pimodule::planrepairs.create resource')}}"><i class="fa fa-wrench"></i></button>
    <button class="btn btn-default btn-flat storno" parameter="{{$record->id}}" title="{{trans('pimodule::planrepairs.storno')}}"><i class="fa fa-remove"></i></button>
    @endif

           
</div>

<div class="modal fade modal-success" id="modal-record-confirmation-{{$equipmentinspection->id}}" tabindex="-1" role="dialog" aria-labelledby="create-confirmation-title" aria-hidden="true">
{!! Form::open(['route' => ['admin.pimodule.record.store',$equipmentinspection->id], 'method' => 'post']) !!}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="create-confirmation-title">{{ trans('core::core.modal.title') }}</h4>
            </div>
            <div class="modal-body">
            @if($planRepairs = $equipmentinspection->planRepair)
            <div class="row">
                <ul>
                @foreach($planRepairs as $planRepair)
                    <li><span>{{ $planRepair->repair_date->format('d.m.Y') }}</span> - <span title="{{ $planRepair->description }}">{{ $planRepair->title }}</span></li>
                @endforeach
                </ul>
            </div>
            @endif
            <div class="row"> 
        		
        			<div class="col-xs-12 center">
        				{!! Form::label('record_status_id', trans('pimodule::records.form.status')) !!}
        			</div>
                	<div class="col-xs-12">
                    	{!! Form::select('record_status_id',$recordstatus,null,['class' => 'form-control','required']) !!}
                    </div>
                    <div class="col-xs-12 center">
        				{!! Form::label('description', trans('pimodule::records.form.description') ) !!}
        			</div>
                	<div class="col-xs-12">
                       {!! Form::textarea('description',null,['class' => 'form-control']) !!}
                    </div>
                	<div class="col-xs-12">
                       {!! Form::normalSelect('user_id', trans('pimodule::records.form.inspector'), $errors, $users, null, ['required']) !!}
                    </div>
                    <div class="col-xs-12 center">
        				{!! Form::label('plan_repair', trans('pimodule::records.form.plan_repair') ) !!}
        			</div>
                    <div class="col-xs-12">
                    	{!! Form::checkbox('plan_repair') !!}
                    </div>
                        {!! Form::hidden('equipmentinspection',$equipmentinspection->id) !!}
                        {!! Form::hidden('equipment_id',$equipmentinspection->equipment_id) !!}
                        {!! Form::hidden('line_id',$equipmentinspection->line_id) !!}
                        {!! Form::hidden('hall_id',$equipmentinspection->hall_id) !!}

                    
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
                
                <button type="submit" class="btn btn-outline btn-flat">{{ trans('pimodule::records.button.create') }}</button>
               
            </div>
        </div>
    </div>
     {!! Form::close() !!}
</div>

<div class="modal fade modal-success" id="modal-plan-repair-{{$equipmentinspection->id}}" tabindex="-1" role="dialog" aria-labelledby="create-confirmation-title" aria-hidden="true">
{!! Form::open(['route' => ['admin.pimodule.planrepair.store',$equipmentinspection->id, $record['id']], 'method' => 'post']) !!}
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="create-confirmation-title">{{ trans('pimodule::planrepairs.modal.title') }}</h4>
            </div>
            <div class="modal-body">
            <div class="row"> 
                
                    <div class="col-xs-12">
                        {!! Form::normalInputOfType('date' ,'repair_date', trans('pimodule::planrepairs.form.repair_date'),$errors,  null, ['class' => 'form-control','required']) !!}
                    </div>
                    <div class="col-xs-12">
                        {!! Form::normalInput('title', trans('pimodule::planrepairs.form.title'),$errors,  null, ['class' => 'form-control','required']) !!}
                    </div>

                    <div class="col-xs-12">
                       {!! Form::normalTextarea('description',trans('pimodule::planrepairs.form.description'), $errors, $equipmentinspection->record()->first(),['class' => 'form-control', 'rows' => 5, 'cols' => 40]) !!}
                    </div>
                    <div class="col-xs-12">
                       {!! Form::normalSelect('user_id', trans('pimodule::planrepairs.form.user_id'), $errors, $users, null, ['required', 'placeholder' => 'Select User']) !!}
                    </div>                    
            </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline btn-flat" data-dismiss="modal">{{ trans('core::core.button.cancel') }}</button>
                
                <button type="submit" class="btn btn-outline btn-flat">{{ trans('pimodule::planrepairs.button.create') }}</button>
               
            </div>
        </div>
    </div>
{!! Form::close() !!}
</div>



