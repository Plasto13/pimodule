<div class="info-box bg-green">
  <span class="info-box-icon"><i class="fa fa-thumbs-o-up"></i></span>

  <div class="info-box-content">
    <span class="info-box-text">TPM Celkovo</span>
    <span class="info-box-number">{{ $equipmentInspeciton }}</span>

    <div class="progress">
      <div class="progress-bar" style="width: {{ ($inspectionProgress/$equipmentInspeciton)*100 }}%"></div>
    </div>
        <span class="progress-description">
          Chybajuce : {{ $inspectionMissing }} {{ $currentUser }}
        </span>
  </div>
  <!-- /.info-box-content -->
</div>