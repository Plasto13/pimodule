<div class="box box-primary">
    <div class="box-header">
        <h3 class="box-title">Planovane opravy celovo -> {{ $count }}</h3>
    </div><!-- /.box-header -->
    <div class="box-body no-padding">
        <table class="table table-striped">
            <tbody><tr>
                <th>datum</th>
                <th>pocet</th>
            </tr>
            @if (isset($groupByDate))
                @foreach ($groupByDate as $date => $value)
                    <tr>
                        <td>{{ $date }}</td>
                        <td>{{ $value->count() }}</td>
                       
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div><!-- /.box-body -->
</div>