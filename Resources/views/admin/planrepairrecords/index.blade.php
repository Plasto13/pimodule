@extends('layouts.master')

@section('content-header')
    <h1>
        {{ trans('pimodule::planrepairrecords.title.planrepairrecords') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li class="active">{{ trans('pimodule::planrepairrecords.title.planrepairrecords') }}</li>
    </ol>
@stop

@section('styles')
<link rel="stylesheet" href="{{asset('themes/adminlte/vendor/admin-lte/plugins/datepicker/datepicker3.css')}}">
<link rel="stylesheet" href="{!! Module::asset('base:css/select2.css') !!}" />
<link rel="stylesheet" href="{!! Module::asset('base:css/select2-bootstrap-dick.css') !!}"/>
@endsection

@push('css-stack')
    
    <style type="text/css">

    table input {
             max-width: 100px;
    }

    .m-b-0 {
    margin-bottom: 0px;
    }

    .base-filter label {
      color: #868686 ;
      font-weight: 600;
      text-transform: uppercase;
    }

    .navbar-filters {
      min-height: 25px !important;
      border-radius: 0 !important;
      margin-bottom: 10px !important;
      margin-left: -10px !important;
      margin-right: -10px!important ;
      margin-top: -11px !important;
      background: #f9f9f9 ;
      border-color: #f4f4f4 ;
    }

    .navbar-filters .navbar-collapse {
        padding: 0 !important;
    }

    .navbar-filters .navbar-toggle {
      padding: 10px 15px !important;
      border-radius: 0 !important;
    }

    .navbar-filters .navbar-brand {
      height: 25px !important;
      padding: 5px 15px !important;
      font-size: 14px !important;
      text-transform: uppercase !important;
    }
    @media (min-width: 768px) {
      .navbar-filters .navbar-nav>li>a {
          padding-top: 5px !important;
          padding-bottom: 5px !important;
      }
    }

    @media (max-width: 768px) {
      .navbar-filters .navbar-nav {
        margin: 0 !important;
      }
    }

    .select2-container {
        display: inline-block !important;
      }
      .select2-drop-active {
        border:none !important;
      }
      .select2-container .select2-choices .select2-search-field input, .select2-container .select2-choice, .select2-container .select2-choices {
        border: none !important;
      }
      .select2-container-active .select2-choice {
        border: none !important;
        box-shadow: none !important;
      }
    </style>
@endpush

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="row">
                <div class="btn-group pull-right" style="margin: 0 15px 15px 0;">
                    <a href="{{ route('admin.pimodule.planrepairrecord.create') }}" class="btn btn-primary btn-flat" style="padding: 4px 10px;">
                        <i class="fa fa-pencil"></i> {{ trans('pimodule::planrepairrecords.button.create planrepairrecord') }}
                    </a>
                </div>
            </div>
            <div class="box box-primary">
                <div class="box-header">
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if (isset($base['filters']))
                        @include('base::inc.filter')
                    @endif
                    <div class="table-responsive">
                        <table class="data-table table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th data-sortable="false">{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if (isset($planrepairrecords)): ?>
                            <?php foreach ($planrepairrecords as $planrepairrecord): ?>
                            <tr>
                                <td>
                                    <a href="{{ route('admin.pimodule.planrepairrecord.edit', [$planrepairrecord->id]) }}">
                                        {{ $planrepairrecord->created_at }}
                                    </a>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('admin.pimodule.planrepairrecord.edit', [$planrepairrecord->id]) }}" class="btn btn-default btn-flat"><i class="fa fa-pencil"></i></a>
                                        <button class="btn btn-danger btn-flat" data-toggle="modal" data-target="#modal-delete-confirmation" data-action-target="{{ route('admin.pimodule.planrepairrecord.destroy', [$planrepairrecord->id]) }}"><i class="fa fa-trash"></i></button>
                                    </div>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            <?php endif; ?>
                            </tbody>
                            <tfoot>
                            <tr>
                                <th>{{ trans('core::core.table.created at') }}</th>
                                <th>{{ trans('core::core.table.actions') }}</th>
                            </tr>
                            </tfoot>
                        </table>
                        <!-- /.box-body -->
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('pimodule::planrepairrecords.title.create planrepairrecord') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "<?= route('admin.pimodule.planrepairrecord.create') ?>" }
                ]
            });
        });
    </script>
    
    <?php $locale = locale(); ?>
    {!! $html->scripts() !!}
    <script src="{{asset('themes/adminlte/vendor/admin-lte/plugins/datepicker/locales/bootstrap-datepicker.sk.js')}}"></script>
    <script src="{{asset('themes/adminlte/vendor/admin-lte/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
    <script src="{!! Module::asset('base:js/select2/select2.js') !!}"></script>
@endpush
