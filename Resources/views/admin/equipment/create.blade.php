@extends('layouts.master')

@push('css-stack')
<link href="{!! Module::asset('pimodule:css/clusterize.css') !!}" rel="stylesheet">
@endpush

@section('content-header')
    <h1>
        {{ trans('pimodule::equipment.title.create equipment') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.pimodule.equipment.index') }}">{{ trans('pimodule::equipment.title.equipment') }}</a></li>
        <li class="active">{{ trans('pimodule::equipment.title.create equipment') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.pimodule.equipment.store'], 'method' => 'post']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                
                <div class="row">
                    <div class="col-md-4">
                        {!! Form::normalInput('title',trans('pimodule::equipment.form.title'), $errors,null,['required']) !!} 
                    </div>
                    <div class="col-md-8">
                        {!! Form::normalInput('description',trans('pimodule::equipment.form.description'), $errors) !!} 
                    </div>
                </div>
                
                    <div class="row table-responsive">
                    <div class="col-md-12">
                        <table class="table table-sm">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th scope="col">{{ trans('pimodule::inspections.title.inspections') }}</th>
                            <th scope="col">{{ trans('user::users.form.last-name') }}</th>
                            <th scope="col">{{ trans('pimodule::equipment.form.name') }}</th>
                            <th scope="col">{{ trans('pimodule::equipment.form.cycle') }}</th>
                        </tr>
                        </thead>
                        </table>
                         <div id="scrollArea" class="clusterize-scroll">
                        <table class="table table-sm">
                        <tbody id="contentArea" class="clusterize-content">
                        <tr class="clusterize-no-data">
                          <td><td>{{ trans('pimodule.equipment.table.loading') }} </td></td>
                        </tr>
                        
                        </tbody>
                        </table>
                        </div>
                    </div>
                    </div>

                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('pimodule::admin.equipment.partials.create-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.create') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.pimodule.equipment.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
<script src="{!! Module::asset('pimodule:js/clusterize.min.js') !!}"></script>

    <script type="text/javascript">
    var data = [];

    $.ajax({
        type: "GET",
        url: '{{ route('admin.pimodule.equipment.inspection.all') }}',
        success: function(data){
            // alert(data[1]);
           var clusterize = new Clusterize({
            rows: data,
            scrollId: 'scrollArea',
            contentId: 'contentArea'
        });
        }
    });
        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.pimodule.equipment.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
