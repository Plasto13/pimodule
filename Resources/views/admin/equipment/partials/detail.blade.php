<div class="box">
                <div class="box-header with-border-header">
                <h3 class="box-title">{{ $model->equipment_name }}</h3>
                </div>
                <!-- /.box-header -->
                <div  class="box-body">
                @if(isset($planrepairs))
                <div class="row">
                    <div class="box-header with-border-header">
                        <h4 class="box-title">{{ trans('pimodule::equipment.detail.repair planed') }} {{ $planrepairs->count() }}</h3>
                    </div>
                
                <ul>
                	@foreach($planrepairs as $planrepair)
                	<li title="{{ $planrepair->description }}">
                		<span>{{ $planrepair->repair_date->format('Y.m.d') }}</span> - <span>{{ $planrepair->title }}</span>
                	</li>
                	@endforeach
            	</ul>
                </div>
                @endif
                @if(@isset($records))
                 <div class="row">
                    <div class="box-header with-border-header">
                        <h4 class="box-title">{{ trans('pimodule::equipment.detail.latest records') }}</h3>
                    </div>
                    <ul>
                    @foreach($records as $record)
                    <li title="{{ $record->description }}">
                        <span>{{ $record->created_at->format('Y.m.d') }}</span> - 
                        <span>{{ $record->user->present()->fullname() }}</span> - 
                        <span>{{ $record->status()->first()->title }}</span> - <span>{{ $record->description }}</span>
                    </li>
                    @endforeach
                </ul>
                </div>                   
                @endif
                </div>
            </div>
