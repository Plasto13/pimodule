@extends('layouts.master')

@push('css-stack')
<link href="{!! Module::asset('pimodule:css/clusterize.css') !!}" rel="stylesheet">
@endpush

@section('content-header')
    <h1>
       @if(class_basename($equipment) == 'Equipment')
        {{ $equipment->line->hall->equipment_name  }} - {{ $equipment->line->equipment_name  }} - {{ $equipment->equipment_name  }}
       @endif
       @if(class_basename($equipment) == 'Line')
        {{ $equipment->hall->equipment_name  }} - {{ $equipment->equipment_name  }}
       @endif
       @if(class_basename($equipment) == 'Hall')
        {{ $equipment->equipment_name  }}
       @endif
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.pimodule.equipment.index') }}">{{ trans('pimodule::equipment.breadcrumb.equipment') }}</a></li>
        <li class="active">{{ trans('pimodule::equipment.breadcrumb.equipment inspection') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.pimodule.equipment.update'], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">

                <div class="row">
                    <div class="col-md-4">
                        {!! Form::normalInput('equipment_name',trans('pimodule::equipment.form.title'), $errors,$equipment,['disabled']) !!} 
                    </div>
                </div>

                
                    <div class="row table-responsive">
                    <div class="col-md-12">
                        <table id="scrollArea"  class="table table-sm clusterize">
                        <thead id="scrollArea" class="clusterize">
                        <tr>

                            <th>#</th>
                            <th scope="col">{{ trans('pimodule::inspections.title.inspections') }}</th>
                            <th scope="col">{{ trans('pimodule::inspections.form.user name') }}</th>
                            <th scope="col">{{ trans('pimodule::equipment.form.cycle') }}</th>
                        </tr>
                        </thead>
                        <tbody id="contentArea" class="clusterize emulate-progress">
                        <tr class="clusterize-no-data" >
                          <td colspan="4">{{ trans('pimodule.equipment.table.loading') }} </td>
                        </tr>
                        
                        </tbody>
                        </table>
                    </div>
                    </div>
                    {!! Form::hidden('equipment_id',isset($equipment_id)  ? $equipment_id : '') !!}
                    {!! Form::hidden('line_id',isset($line_id) ? $line_id : '') !!}
                    {!! Form::hidden('hall_id',isset($hall_id) ? $hall_id : '') !!}

                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('pimodule::admin.equipment.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.pimodule.equipment.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
<script src="{!! Module::asset('pimodule:js/clusterize.min.js') !!}"></script>

    <script type="text/javascript">
    var data = [];
    var route;
    @if (isset($equipment_id))
    route = '{{ route('admin.pimodule.equipment.inspections','e='.$equipment_id) }}';
    @endif
    @if (isset($line_id))
    route = '{{ route('admin.pimodule.equipment.inspections','l='.$line_id) }}';
    @endif
    @if (isset($hall_id))
    route = '{{ route('admin.pimodule.equipment.inspections','h='.$hall_id) }}';
    @endif

    $.ajax({
        type: "GET",
        url: route,
        success: function(data){
            // alert(data[1]);
           var clusterize = new Clusterize({
            rows: data,
            scrollId: 'scrollArea',
            contentId: 'contentArea',
        });
        }
    });
    

        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "{{ route('admin.pimodule.equipment.index') }}" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush