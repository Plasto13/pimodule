@extends('layouts.master')

@push('css-stack')
    <link href="{!! Module::asset('base:css/nestable.css') !!}" rel="stylesheet" type="text/css" />
@endpush

@section('content-header')
    <h1>
        {{ trans('base::halls.title.halls') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.pimodule.equipmentinspection.index') }}">{{ trans('pimodule::equipmentinspections.title.equipmentinspections') }}</a></li>
        <li class="active">{{ trans('base::halls.title.halls') }}</li>
    </ol>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-6">
            <div class="box box-primary" style="overflow: hidden;">
                <div class="box-header">
                  
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="dd">
                       
                                @if($halls)
                                @foreach($halls as $hall)
                                <ol class="dd-list" id="hall{{$hall->id}}">
                                    <li class="dd-item">
                                        <button data-action="collapse" type="button" data-target="#hall{{$hall->id}}">Collapse</button>
                                        <div role="group" aria-label="Action buttons" class="btn-group" style="display: inline;">
                                            <a href="{{ route('admin.pimodule.equipment.edit', ['h='.$hall->id]) }}" class="btn btn-sm btn-info" style="float: left;margin-right: 15px;">
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </div>
                                        <div parameter="{{$hall->id}}" hall="{{$hall->id}}" class="dd-handle">
                                            {{ $hall->equipment_name}}
                                         </div>
                                        @if($lines = $hall->line()->get())
                                        @foreach($lines as $line)
                                        <ol class="dd-list sub" style="display: none;">
                                            <li class="dd-item">
                                                <div role="group" aria-label="Action buttons" class="btn-group" style="display: inline;">
                                                    <a href="{{ route('admin.pimodule.equipment.edit', ['l='.$line->id]) }}" class="btn btn-sm btn-info" style="float: left;margin-right: 15px;">
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                    
                                                </div>
                                                <div parameter="{{ $line->id}}" line="{{ $line->id}}" class="dd-handle">
                                                    {{ $line->equipment_name}}
                                                </div>
                                            </li>
                                            @if($equipments = $line->equipment()->get())
                                            @foreach($equipments as $equipment)
                                            <ol class="dd-list">
                                                <li class="dd-item">
                                                    <div role="group" aria-label="Action buttons" class="btn-group" style="display: inline;">
                                                        <a href="{{ route('admin.pimodule.equipment.edit', ['e='.$equipment->id]) }}" class="btn btn-sm btn-info" style="float: left;">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <button parameter="{{ $equipment->id }}" class="btn btn-sm btn-info generate" style="float: left;margin-right: 15px;">
                                                            <i class="fa fa-spinner"></i>
                                                        </button>
                                                    </div>
                                                    <div parameter="{{ $equipment->id}}" equipment="{{ $equipment->id}}" class="dd-handle">
                                                        {{ $equipment->equipment_name}}</div></li>
                                            </ol>
                                            @endforeach
                                            @endif
                                        </ol>
                                        @endforeach
                                        @endif
                                    </li>
                                </ol>
                                @endforeach
                                @endif
                            
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
        <div id="detail" class="col-xs-6">
            
        </div>
    </div>
    @include('core::partials.delete-modal')
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>c</code></dt>
        <dd>{{ trans('base::halls.title.create hall') }}</dd>
    </dl>
@stop

@push('js-stack')
    <script type="text/javascript">
        $( document ).ready(function() {
            var _token = $("input[name='_token']").val();
            $(document).keypressAction({
                actions: [
                    { key: 'c', route: "{{ route('admin.base.hall.create') }}" }
                ]
            });
            $('div[hall]').click(function() {
                var id = $(this).attr('parameter');
                $.ajax(
                    {
                      url: " {{ route('api.pimodule.equipment.detail') }} ",
                      type: 'POST',
                      data : {'id': id,
                          '_token': _token, 
                          'model':'hall',
                      },
                      success: function(data){
                        $('#detail').html(data);
                      },
                      error: function(response){
                        alert('Error'+response);
                      }
                    }
                  ); 
                return false;               
              });
            $('div[line]').click(function() {
                var id = $(this).attr('parameter');
                $.ajax(
                    {
                      url: "{{ route('api.pimodule.equipment.detail') }}",
                      type: 'POST',
                      data : {'id': id,
                          '_token': _token, 
                          'model':'line',
                      },
                      success: function(data){
                        $('#detail').html(data);
                      },
                      error: function(response){
                        alert('Error'+response);
                      }
                    }
                  ); 
                return false;
              });
            $('div[equipment]').click(function() {
                var id = $(this).attr('parameter');
                 $.ajax(
                    {
                      url: "{{ route('api.pimodule.equipment.detail') }}",
                      type: 'POST',
                      data : {'id': id,
                          '_token': _token, 
                          'model':'equipment'
                      },
                      success: function(data){

                        $('#detail').html(data);
                      },
                      error: function(response){
                        alert('Error'+response);
                      }
                    }
                  ); 
                return false;
              });
            $('.generate').click(function() {
                var id = $(this).attr('parameter');
                console.log(id);
                 $.ajax(
                    {
                      url: "{{route('api.base.equipment.api.generate')}}",
                      type: 'POST',
                       data : {'id': id
                      },
                      success: function(data){
                        console.log(data);
                        alert(data.message);
                      },
                      error: function(response){
                        console.log('Error'+response);
                      }
                    }
                  ); 
                return false;
              });
            $('.dd-list > li button').click(function() {
              $(this).parent().find('.sub').toggle();
              console.log("klik");
            });
          });
    </script>

@endpush
