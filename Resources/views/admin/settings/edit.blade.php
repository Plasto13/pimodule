@extends('layouts.master')

@push('css-stack')
<link rel="stylesheet" type="text/css" href="{!! Module::asset('pimodule:css/bootstrap-colorpicker.min.css') !!}">
@endpush

@section('content-header')
    <h1>
        {{ trans('pimodule::settings.title.edit settings') }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard.index') }}"><i class="fa fa-dashboard"></i> {{ trans('core::core.breadcrumb.home') }}</a></li>
        <li><a href="{{ route('admin.pimodule.settings.index') }}">{{ trans('pimodule::settings.title.settings') }}</a></li>
        <li class="active">{{ trans('pimodule::settings.title.edit settings') }}</li>
    </ol>
@stop

@section('content')
    {!! Form::open(['route' => ['admin.pimodule.settings.update', $settings->id], 'method' => 'put']) !!}
    <div class="row">
        <div class="col-md-12">
            <div class="nav-tabs-custom">
                @include('partials.form-tab-headers')
                <div class="tab-content">
                    <div class="col-md-2">
                        {!! Form::label('prewarning_days',trans('pimodule::settings.form.prewarning_days')) !!}
                        {!! Form::number('prewarning_days', $settings->prewarning_days, ['class' => 'form-control']) !!}
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group color-picker">
                            {!! Form::label('prewarning_color',trans('pimodule::settings.form.prewraning_color')) !!}
                                <div class="input-group color-picker">
                                {!! Form::text('prewarning_color',$settings->prewarning_color, ['class' => 'form-control']) !!}
                                    <div class="input-group-addon">
                                    <i></i>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group color-picker">
                            {!! Form::label('warning_color',trans('pimodule::settings.form.warning_color')) !!}
                                <div class="input-group color-picker">
                                {!! Form::text('warning_color',$settings->warning_color, ['class' => 'form-control']) !!}
                                    <div class="input-group-addon">
                                    <i></i>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="input-group color-picker">
                            {!! Form::label('repair_color',trans('pimodule::settings.form.repair_color')) !!}
                                <div class="input-group color-picker">
                                {!! Form::text('repair_color',$settings->repair_color, ['class' => 'form-control']) !!}
                                    <div class="input-group-addon">
                                    <i></i>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <?php $i = 0; ?>
                    @foreach (LaravelLocalization::getSupportedLocales() as $locale => $language)
                        <?php $i++; ?>
                        <div class="tab-pane {{ locale() == $locale ? 'active' : '' }}" id="tab_{{ $i }}">
                            @include('pimodule::admin.settings.partials.edit-fields', ['lang' => $locale])
                        </div>
                    @endforeach

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-flat">{{ trans('core::core.button.update') }}</button>
                        <a class="btn btn-danger pull-right btn-flat" href="{{ route('admin.pimodule.settings.index')}}"><i class="fa fa-times"></i> {{ trans('core::core.button.cancel') }}</a>
                    </div>
                </div>
            </div> {{-- end nav-tabs-custom --}}
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('footer')
    <a data-toggle="modal" data-target="#keyboardShortcutsModal"><i class="fa fa-keyboard-o"></i></a> &nbsp;
@stop
@section('shortcuts')
    <dl class="dl-horizontal">
        <dt><code>b</code></dt>
        <dd>{{ trans('core::core.back to index') }}</dd>
    </dl>
@stop

@push('js-stack')
<script src="{!! Module::asset('pimodule:js/bootstrap-colorpicker.min.js') !!}"></script>
    <script type="text/javascript">
        $('.color-picker').colorpicker()

        $( document ).ready(function() {
            $(document).keypressAction({
                actions: [
                    { key: 'b', route: "<?= route('admin.pimodule.settings.index') ?>" }
                ]
            });
        });
    </script>
    <script>
        $( document ).ready(function() {
            $('input[type="checkbox"].flat-blue, input[type="radio"].flat-blue').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });
        });
    </script>
@endpush
