
@if(isset($record->status()->first()->title))
	@if(!$record->planed && $record->plan_repair)
		<span class="alert-error label">
	@elseif(!$record->planed && !$record->plan_repair)
		<span class="alert-success label">
	@elseif($record->planed && $record->plan_repair)
		<span class="alert-warning label">
	@endif
{{ $record->status()->first()->title }}</span>
</span>
@endif
