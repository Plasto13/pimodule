<?php

return [
    'list resource' => 'List planrepairs',
    'create resource' => 'Create planrepairs',
    'edit resource' => 'Edit planrepairs',
    'destroy resource' => 'Destroy planrepairs',
    'title' => [
        'planrepairs' => 'PlanRepair',
        'create planrepair' => 'Create a planrepair',
        'edit planrepair' => 'Edit a planrepair',
    ],
    'button' => [
        'create planrepair' => 'Create a planrepair',
         'timeline' => 'Timeline',
    ],
    'table' => [
        'action' => 'Action',
        'repair_date' => 'Repair date',
        'finished' => 'finished',
        'yes' => 'Yes',
        'no' => 'no',
    ],
    'form' => [
        'repair_date' => 'Date',
        'title' => 'Title',
        'description' => 'Description',
        'user_id' => 'User',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
