<?php

return [
    'list resource' => 'List weekendplans',
    'create resource' => 'Create weekendplans',
    'edit resource' => 'Edit weekendplans',
    'destroy resource' => 'Destroy weekendplans',
    'title' => [
        'weekendplans' => 'WeekendPlan',
        'create weekendplan' => 'Create a weekendplan',
        'edit weekendplan' => 'Edit a weekendplan',
    ],
    'button' => [
        'create weekendplan' => 'Create a weekendplan',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
