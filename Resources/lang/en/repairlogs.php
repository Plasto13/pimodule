<?php

return [
    'list resource' => 'List repairlogs',
    'create resource' => 'Create repairlogs',
    'edit resource' => 'Edit repairlogs',
    'destroy resource' => 'Destroy repairlogs',
    'title' => [
        'repairlogs' => 'RepairLog',
        'create repairlog' => 'Create a repairlog',
        'edit repairlog' => 'Edit a repairlog',
    ],
    'button' => [
        'create repairlog' => 'Create a repairlog',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
