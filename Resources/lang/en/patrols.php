<?php

return [
    'list resource' => 'List patrols',
    'create resource' => 'Create patrols',
    'edit resource' => 'Edit patrols',
    'destroy resource' => 'Destroy patrols',
    'title' => [
        'patrols' => 'Patrol',
        'create patrol' => 'Create a patrol',
        'edit patrol' => 'Edit a patrol',
    ],
    'button' => [
        'create patrol' => 'Create a patrol',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
