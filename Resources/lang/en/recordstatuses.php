<?php

return [
    'list resource' => 'List statuses',
    'create resource' => 'Create statuses',
    'edit resource' => 'Edit statuses',
    'destroy resource' => 'Destroy statuses',
    'title' => [
        'recordstatuses' => 'Stavy Kontrol',
        'create recordstatus' => 'Create a status',
        'edit recordstatus' => 'Edit a status',
    ],
    'button' => [
        'create recordstatus' => 'Create a status',
        'recordstatuses' => 'Stavy Kontrol',
    ],
    'table' => [
        'title' => 'Status',
        'description' => 'Description',
    ],
    'form' => [
        'title' => 'Status',
        'description' => 'Description',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
