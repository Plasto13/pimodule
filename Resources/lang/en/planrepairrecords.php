<?php

return [
    'list resource' => 'List planrepairrecords',
    'create resource' => 'Create planrepairrecords',
    'edit resource' => 'Edit planrepairrecords',
    'destroy resource' => 'Destroy planrepairrecords',
    'title' => [
        'planrepairrecords' => 'PlanRepairRecord',
        'create planrepairrecord' => 'Create a planrepairrecord',
        'edit planrepairrecord' => 'Edit a planrepairrecord',
    ],
    'button' => [
        'create planrepairrecord' => 'Create a planrepairrecord',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
