<?php

return [
    'list resource' => 'List records',
    'create resource' => 'Create records',
    'edit resource' => 'Edit records',
    'destroy resource' => 'Destroy records',
    'title' => [
        'records' => 'Record',
        'create record' => 'Create a record',
        'edit record' => 'Edit a record',
    ],
    'button' => [
        'create record' => 'Create a record',
        'create' => 'Save',
    ],
    'table' => [
        'id' => 'ID',
        'equipment_inspection_id' => 'Inspection',
        'plan_repair' => 'Plan repair',
        'planned' => 'Planned',
        'repaired' => 'Repaired',
        'record_status_id' => 'Status',
        'description' => 'Description',
        'inspector' => 'Inspector',
        'equipment' => 'Equipment',
        'inspection' => 'Inspection',
        'created_at' => 'Created at',
        'action' => 'Action',
        'yes' => 'Yes',
        'no' => 'No',
    ],
    'form' => [
        'plan_repair' => 'Plan repair',
        'status' => 'Status',
        'select status' => 'Select status',
        'inspector' => 'Inspector',
        'select inspector' => 'Select inspector',
        'description' => 'Description',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
