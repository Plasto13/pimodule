<?php

return [
    'list resource' => 'List inspections',
    'create resource' => 'Create inspections',
    'edit resource' => 'Edit inspections',
    'destroy resource' => 'Destroy inspections',
    'title' => [
        'inspections' => 'Inspection',
        'create inspection' => 'Create a inspection',
        'edit inspection' => 'Edit a inspection',
    ],
    'button' => [
        'create inspection' => 'Create a inspection',
    ],
    'table' => [
        'id' => 'ID',
        'title' => 'Title',
        'description' => 'Description',
        'cycle' => 'Cycle',
        'action' => 'Action'
    ],
    'form' => [
        'title' => 'Title',
        'description' => 'Description',
        'cycle' => 'Cycle in dayPs'
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
