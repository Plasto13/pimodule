<?php

return [
    'list resource' => 'List equipment',
    'create resource' => 'Create equipment',
    'edit resource' => 'Edit equipment',
    'destroy resource' => 'Destroy equipment',
    'title' => [
        'equipment' => 'Equipment',
        'create equipment' => 'Create a equipment',
        'edit equipment' => 'Edit a equipment',
    ],
    'button' => [
        'create equipment' => 'Create a equipment',
    ],
    'table' => [
    ],
    'form' => [
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'breadcrumb' => [
        'equipment' => 'Equipment',
        'equipment inspection' => 'Device checks',
    ],
];
