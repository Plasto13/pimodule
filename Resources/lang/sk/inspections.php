<?php

return [
    'list resource' => 'List inspections',
    'create resource' => 'Create inspections',
    'edit resource' => 'Edit inspections',
    'destroy resource' => 'Destroy inspections',
    'title' => [
        'inspections' => 'Kontroly',
        'inspection' => 'Kontrola',
        'create inspection' => 'Vytovriť kontrolu',
        'edit inspection' => 'Upraviť kontrolu',
    ],
    'button' => [
        'create inspection' => 'Nová kontrola',
        'inspections' => 'Kontroly',
        'import items' => 'Import',
    ],
    'table' => [
        'id' => 'ID',
        'title' => 'Názov',
        'description' => 'Popis',
        'cycle' => 'Cyklus',
        'action' => 'Akcia'
    ],
    'form' => [
        'title' => 'Názov',
        'description' => 'Popis',
        'cycle' => 'Cyklus v dňoch',
        'user name' => "Meno",
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
