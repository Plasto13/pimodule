<?php

return [
    'list resource' => 'List planrepairs',
    'create resource' => 'Create planrepairs',
    'edit resource' => 'Edit planrepairs',
    'destroy resource' => 'Destroy planrepairs',
    'title' => [
        'planrepairs' => 'Plánované opravy',
        'create planrepair' => 'Create a planrepair',
        'edit planrepair' => 'Edit a planrepair',
    ],
    'button' => [
        'create planrepair' => 'Create a planrepair',
        'timeline' => 'Časová línia',
        'planrepairs' => 'Plánované opravy',
    ],
    'table' => [
        'action' => 'Akcia',
        'repair_date' => 'Dátum opravy',
        'finished' => 'Ukončené',
        'user_id' => 'Zodpovedný',
        'yes' => 'Ano',
        'no' => 'Nie',
    ],
    'form' => [
        'repair_date' => 'Datum opravy',
        'title' => 'Názov',
        'description' => 'Popis',
        'user_id' => 'Zodpovedný',
    ],
    'filter' => [
        'planrepairs' => 'Naplanovat',
        'planed' => 'Naplanované',
        'ended' => 'Ukoncene',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
