<?php

return [
    'list resource' => 'Prehľad zariadení',
    'create resource' => 'Vytovriť zariadenie',
    'edit resource' => 'Upraviť zariadenie',
    'destroy resource' => 'Destroy equipment',
    'title' => [
        'equipment' => 'Zariadenia',
        'equipment_1' => 'Zariadenie',
        'create equipment' => 'Vytovriť zariadenie',
        'edit equipment' => 'Upraviť zariadenie',
    ],
    'button' => [
        'create equipment' => 'Nové zariadenie',
    ],
    'table' => [
        'id' => 'Id',
        'title' => 'Zariadenie',
        'code' => 'Kód',
        'description' => 'Popis',
        'action' => 'Akcia',
    ],
    'form' => [
        'title' => 'Názov',
        'description' => 'Popis',
        'name' => 'Meno',
        'cycle' => 'Cyklus v dňoch'
    ],
    'detail' => [
        'repair planed' => 'Planované opravy',
        'latest records' => 'Najnovšie záznamy',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
    'breadcrumb' => [
        'equipment' => 'Zariadenia',
        'equipment inspection' => 'Kontroly zariadenia',
    ],
];
