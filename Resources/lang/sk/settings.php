<?php

return [
    'list resource' => 'List settings',
    'create resource' => 'Create settings',
    'edit resource' => 'Edit settings',
    'destroy resource' => 'Destroy settings',
    'title' => [
        'settings' => 'Settings',
        'create settings' => 'Create a settings',
        'edit settings' => 'Edit a settings',
    ],
    'button' => [
        'create settings' => 'Create a settings',
    ],
    'table' => [
        'filters' => 'Filter',
        'remove_filters' => 'Clear filter'
    ],
    'form' => [
        'prewarning_days' => 'Prewaning in days',
        'prewraning_color' => 'Prewaning color',
        'warning_color' => 'Warning color',
        'repair_color'=> 'Plan repair color',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
