<?php

return [
    'list resource' => 'List equipmentinspections',
    'create resource' => 'Create equipmentinspections',
    'edit resource' => 'Edit equipmentinspections',
    'destroy resource' => 'Destroy equipmentinspections',
    'title' => [
        'equipmentinspections' => 'EquipmentInspection',
        'create equipmentinspection' => 'Create a equipmentinspection',
        'edit equipmentinspection' => 'Edit a equipmentinspection',
    ],
    'button' => [
        'create equipmentinspection' => 'Create a equipmentinspection',
        'export equipmentinspection' => 'Export inspection',
    ],
    'table' => [
        'id' => 'Id',
        'equipment' => 'Equipmnet',
        'inspection' => 'Inspectinon',
        'inspection description' => 'Popis kontroly',
        'user' => 'User',
        'user_name' => 'Name',
        'cycle' => 'Cycle',
        'last_check' => 'Last check',
        'next_check' => 'Next check',
        'action' => 'Action',
        'none' => 'None',
        'plan_repair' => 'Plan Repair',
        'instruction' => 'Inštrukcia',
        'last_status' => 'Last Status'

    ],
    'form' => [
        'export title' => 'Export',
        'export close' => 'Close',
        'export submit' => 'Submit',
        'export from' => 'From',
        'export to' => 'To',
    ],
    'messages' => [
        'planrepair storno ok' => 'planrepair storno ok',
        'planrepair storno nok' => 'planrepair storno nok',
    ],
    'validation' => [
    ],
];
