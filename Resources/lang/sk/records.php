<?php

return [
    'list resource' => 'List records',
    'create resource' => 'Create records',
    'edit resource' => 'Edit records',
    'destroy resource' => 'Destroy records',
    'title' => [
        'records' => 'Záznamy',
        'record' => 'Záznamy',
        'create record' => 'Nový záznam',
        'edit record' => 'Upraviť záznam',
    ],
    'button' => [
        'create record' => 'Nový záznam',
        'records' => 'Záznamy',
    ],
    'table' => [
        'id' => 'ID',
        'equipment_inspection_id' => 'Kontrola',
        'plan_repair' => 'Naplánovať opravu',    
        'planned' => 'Planované',
        'repaired' => 'Opravené',
        'record_status_id' => 'Stav',
        'description' => 'Poznámky',
        'inspector' => 'Kontroloval',
        'equipment' => 'Zariadenie',
        'inspection' => 'Kontrola',
        'created_at' => 'Vytvorene',
        'action' => 'Akcia',
        'yes' => 'Ano',
        'no' => 'Nie',
    ],
    'form' => [
        'plan_repair' => 'Naplánovať opravu?',
        'status' => 'Stav',
        'select status' => 'Vyber stav',
        'inspector' => 'Kontrolór',
        'select inspector' => 'Vyber kontrolóra',
        'description' => 'Poznámky',
        'ended' => 'Ukoncene',
    ],
    'messages' => [
    ],
    'validation' => [
    ],
];
