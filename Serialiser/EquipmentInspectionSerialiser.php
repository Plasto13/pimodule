<?php

namespace Modules\Pimodule\Serialiser;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;
use Modules\Base\Entities\Equipment;
use Modules\Pimodule\Entities\Inspection;
use Modules\User\Entities\Sentinel\User;

class EquipmentInspectionSerialiser implements SerialiserInterface
{

    public function getData($data)
    {
        $equipment = Equipment::find($data->equipment_id);
        // $line = Line::find($data->equipment_id);
        // $hall = Hall::find($data->equipment_id);
        $inspection = Inspection::find($data->inspection_id);
        $user = User::find($data->user_id);
        $row = [];    
        
        $row[] = ($equipment) ? $equipment->line->hall->equipment_name.' - '.$equipment->line->equipment_name.' - '.$equipment->equipment_name : '';
        $row[] = ($inspection) ? $inspection->title : '';
        $row[] = $inspection->description;
        $row[] = ($user) ? $user->first_name.' '.$user->last_name : '';
        $row[] = ($data->last_check) ? Carbon::createFromFormat('Y-m-d H:i:s',$data->last_check)->format('d.m.Y') : '';
        $row[] = ($data->next_check) ? Carbon::createFromFormat('Y-m-d H:i:s',$data->next_check)->format('d.m.Y') : '';

        return $row;
    }

    public function getHeaderRow()
    {
        $header = ['Kde','Kontrola','Popis','Kto','Posledna kontrola','Skontroluj do'];
       return $header;
    }
}

