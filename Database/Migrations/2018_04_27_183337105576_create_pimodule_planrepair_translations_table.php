<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimodulePlanRepairTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__planrepair_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('planrepair_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['planrepair_id', 'locale']);
            $table->foreign('planrepair_id')->references('id')->on('pimodule__planrepairs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pimodule__planrepair_translations', function (Blueprint $table) {
            $table->dropForeign(['planrepair_id']);
        });
        Schema::dropIfExists('pimodule__planrepair_translations');
    }
}
