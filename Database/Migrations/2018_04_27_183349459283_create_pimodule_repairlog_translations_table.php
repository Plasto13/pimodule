<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleRepairLogTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__repairlog_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('repairlog_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['repairlog_id', 'locale']);
            $table->foreign('repairlog_id')->references('id')->on('pimodule__repairlogs')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pimodule__repairlog_translations', function (Blueprint $table) {
            $table->dropForeign(['repairlog_id']);
        });
        Schema::dropIfExists('pimodule__repairlog_translations');
    }
}
