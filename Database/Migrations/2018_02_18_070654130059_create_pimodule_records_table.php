<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__records', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('record_status_id')->unsigned()->nullable();
            $table->integer('equipment_inspection_id')->unsigned()->nullable();
            $table->integer('equipment_id')->unsigned()->nullable();
            $table->integer('inspection_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('equipment')->nullable();
            $table->string('inspection')->nullable();
            $table->string('status')->nullable();
            $table->text('description')->nullable();
            $table->boolean('plan_repair')->nullable();
            $table->string('inspector')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('equipment_inspection_id')->references('id')->on('pimodule__equipment_inspection')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pimodule__records');
    }
}
