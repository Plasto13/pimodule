<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleRecordTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__record_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('record_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['record_id', 'locale'],'pimodule__record_translations');
            $table->foreign('record_id','pimodule__record_translations')->references('id')->on('pimodule__records')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('pimodule__record_translations', function (Blueprint $table) {
        //     $table->dropForeign(['record_id']);
        // });
        Schema::dropIfExists('pimodule__record_translations');
    }
}
