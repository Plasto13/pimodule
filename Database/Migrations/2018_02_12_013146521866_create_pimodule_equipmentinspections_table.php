<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleEquipmentInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__equipment_inspection', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('hall_id')->unsigned()->nullable();
            $table->integer('line_id')->unsigned()->nullable();
            $table->integer('equipment_id')->unsigned()->nullable();
            $table->integer('inspection_id')->unsigned();
            $table->integer('cycle')->nullable();
            $table->string('user_name')->nullable();
            $table->dateTime('last_check')->nullable();
            $table->dateTime('next_check')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('hall_id')->references('id')->on('base__halls')->onDelete('cascade');
            $table->foreign('line_id')->references('id')->on('base__lines')->onDelete('cascade');
            $table->foreign('equipment_id')->references('id')->on('base__equipment')->onDelete('cascade');
            $table->foreign('inspection_id')->references('id')->on('pimodule__inspections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('pimodule__equipment_inspection', function (Blueprint $table) {
        //     $table->dropForeign(['equipment_id','inspection_id']);
        // });
        Schema::dropIfExists('pimodule__equipment_inspection');
    }
}
