<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleWeekendPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__weekendplans', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('equipment_id')->unsigned();
            $table->integer('plan_repair_id')->unsigned();
            $table->text('users')->nullable();
            $table->string('title');
            $table->text('description')->nullable();
            $table->text('remarks')->nullable();
            $table->date('start_date');
            $table->date('end_date')->nullable();
            $table->unsignedInteger('estimated_time')->nullable();
            $table->text('extra')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('equipment_id')->references('id')->on('base__equipment')->onDelete('cascade');
            $table->foreign('plan_repair_id')->references('id')->on('pimodule__planrepairs')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pimodule__weekendplans');
    }
}
