<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPimoduleRecordPlanedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pimodule__records', function (Blueprint $table) {
            $table->boolean('planed')->nullable()->after('plan_repair');
            $table->boolean('finished')->nullable()->after('planed');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pimodule__records', function (Blueprint $table) {
            $table->dropColumn(['planed', 'finished']);
        });
    }
}
