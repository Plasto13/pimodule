<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimodulePlanRepairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__planrepairs', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('record_id')->unsigned();
            $table->integer('equipment_inspection_id')->unsigned();
            $table->date('repair_date');
            $table->string('description')->nullable();
            $table->text('extra')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('record_id')->references('id')->on('pimodule__records')->onDelete('cascade');
            $table->foreign('equipment_inspection_id')->references('id')->on('pimodule__equipment_inspection')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pimodule__planrepairs');
    }
}
