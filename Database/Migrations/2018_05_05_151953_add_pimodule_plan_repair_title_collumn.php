<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPimodulePlanRepairTitleCollumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pimodule__planrepairs', function (Blueprint $table) {
            $table->string('title')->nullable()->after('repair_date');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pimodule__planrepairs', function (Blueprint $table) {
            $table->dropColumn('title');

        });
    }
}
