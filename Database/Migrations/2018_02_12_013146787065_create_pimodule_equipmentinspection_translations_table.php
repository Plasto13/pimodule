<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleEquipmentInspectionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__equipment_inspection_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('equipmentinspection_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['equipmentinspection_id', 'locale'],'pimodule__inspection_translations');
            $table->foreign('equipmentinspection_id','pimodule__inspection_translations')->references('id')->on('pimodule__equipment_inspection')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('pimodule__equipment_inspection_translations', function (Blueprint $table) {
        //     $table->dropForeign(['pimodule__inspection_translations']);
        // });
        Schema::dropIfExists('pimodule__equipment_inspection_translations');
    }
}
