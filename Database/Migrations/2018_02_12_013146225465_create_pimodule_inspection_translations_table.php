<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleInspectionTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__inspection_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title');
            $table->text('description')->nullable();

            $table->integer('inspection_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['inspection_id', 'locale']);
            $table->foreign('inspection_id')->references('id')->on('pimodule__inspections')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pimodule__inspection_translations', function (Blueprint $table) {
            $table->dropForeign(['inspection_id']);
        });
        Schema::dropIfExists('pimodule__inspection_translations');
    }
}
