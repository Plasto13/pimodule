<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleWeekendPlanTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__weekendplan_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('weekendplan_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['weekendplan_id', 'locale']);
            $table->foreign('weekendplan_id')->references('id')->on('pimodule__weekendplans')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pimodule__weekendplan_translations', function (Blueprint $table) {
            $table->dropForeign(['weekendplan_id']);
        });
        Schema::dropIfExists('pimodule__weekendplan_translations');
    }
}
