<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimodulePatrolTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__patrol_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('patrol_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['patrol_id', 'locale']);
            $table->foreign('patrol_id')->references('id')->on('pimodule__patrols')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pimodule__patrol_translations', function (Blueprint $table) {
            $table->dropForeign(['patrol_id']);
        });
        Schema::dropIfExists('pimodule__patrol_translations');
    }
}
