<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePimoduleRecordStatusTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pimodule__recordstatus_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            // Your translatable fields

            $table->integer('recordstatus_id')->unsigned();
            $table->string('locale')->index();
            $table->unique(['recordstatus_id', 'locale'],'recordstatus_translation_id');
            $table->foreign('recordstatus_id','recordstatus_translation_id')->references('id')->on('pimodule__recordstatuses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::table('pimodule__recordstatus_translations', function (Blueprint $table) {
        //     $table->dropForeign(['recordstatus_id']);
        // });
        Schema::dropIfExists('pimodule__recordstatus_translations');
    }
}
