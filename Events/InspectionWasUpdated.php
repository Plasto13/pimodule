<?php

namespace Modules\Pimodule\Events;

use Modules\Media\Contracts\StoringMedia;
use Modules\Pimodule\Entites\Inspection;

class InspectionWasUpdated implements StoringMedia
{

     /**
     * @var Inspection
     */
    private $inspection;

    /**
     * @var array
     */
    private $data;
    
    public function __construct($inspection, array $data)
    {
        $this->inspection = $inspection;
        $this->data = $data;
    }

     /**
     * Return the entity
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getEntity()
    {
        return $this->inspection;
    }
    /**
     * Return the ALL data sent
     * @return array
     */
    public function getSubmissionData()
    {
        return $this->data;
    }
}
