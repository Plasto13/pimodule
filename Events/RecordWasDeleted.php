<?php

namespace Modules\Pimodule\Events;

use Modules\Media\Contracts\DeletingMedia;
use Modules\Pimodule\Entities\Record;

class RecordWasDeleted implements DeletingMedia
{
     /**
     * @var Record
     */
    private $item;

    public function __construct(Record $item)
    {
        $this->item = $item;
    }
    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId()
    {
        return $this->item->id;
    }
    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName()
    {
        return get_class($this->item);
    }
}
