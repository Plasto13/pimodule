<?php

namespace Modules\Pimodule\Events;

use Modules\Media\Contracts\DeletingMedia;
use Modules\Pimodule\Entites\Inspection;

class InspectionWasDeleted implements DeletingMedia
{

      /**
     * @var Inspection
     */
    private $inspection;

    public function __construct($inspection)
    {
        $this->inspection = $inspection;
    }

    /**
     * Get the entity ID
     * @return int
     */
    public function getEntityId()
    {
        return $this->inspection->id;
    }
    
    /**
     * Get the class name the imageables
     * @return string
     */
    public function getClassName()
    {
        return get_class($this->inspection);
    }
}
