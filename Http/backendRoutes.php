<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/pimodule'], function (Router $router) {
    $router->bind('inspection', function ($id) {
        return app('Modules\Pimodule\Repositories\InspectionRepository')->find($id);
    });
    $router->get('inspections', [
        'as' => 'admin.pimodule.inspection.index',
        'uses' => 'InspectionController@index',
        'middleware' => 'can:pimodule.inspections.index'
    ]);
    $router->get('inspections/equipment/all',[
        'as' => 'admin.pimodule.inspection.equipment.all',
        'uses' => 'InspectionController@all',
        'middleware' => 'can:pimodule.inspections.create',
    ]);
    $router->get('inspections/create', [
        'as' => 'admin.pimodule.inspection.create',
        'uses' => 'InspectionController@create',
        'middleware' => 'can:pimodule.inspections.create'
    ]);
    $router->post('inspections', [
        'as' => 'admin.pimodule.inspection.store',
        'uses' => 'InspectionController@store',
        'middleware' => 'can:pimodule.inspections.create'
    ]);
    $router->post('inspections/import', [
        'as' => 'admin.pimodule.inspection.import',
        'uses' => 'InspectionController@import',
        'middleware' => 'can:pimodule.inspections.create'
    ]);
    $router->get('inspections/{inspection}/edit', [
        'as' => 'admin.pimodule.inspection.edit',
        'uses' => 'InspectionController@edit',
        'middleware' => 'can:pimodule.inspections.edit'
    ]);
    $router->put('inspections/{inspection}', [
        'as' => 'admin.pimodule.inspection.update',
        'uses' => 'InspectionController@update',
        'middleware' => 'can:pimodule.inspections.edit'
    ]);
    $router->delete('inspections/{inspection}', [
        'as' => 'admin.pimodule.inspection.destroy',
        'uses' => 'InspectionController@destroy',
        'middleware' => 'can:pimodule.inspections.destroy'
    ]);
    $router->get('inspections/{inspection}/equipment', [
        'as' => 'admin.pimodule.inspection.equipment',
        'uses' => 'InspectionController@equipment',
        'middleware' => 'can:pimodule.inspections.edit'
    ]);
    $router->bind('equipmentinspection', function ($id) {
        return app('Modules\Pimodule\Repositories\EquipmentInspectionRepository')->find($id);
    });
    $router->get('equipmentinspections', [
        'as' => 'admin.pimodule.equipmentinspection.index',
        'uses' => 'EquipmentInspectionController@index',
        'middleware' => 'can:pimodule.equipmentinspections.index'
    ]);
    $router->get('equipmentinspections/create', [
        'as' => 'admin.pimodule.equipmentinspection.create',
        'uses' => 'EquipmentInspectionController@create',
        'middleware' => 'can:pimodule.equipmentinspections.create'
    ]);
    $router->post('equipmentinspections', [
        'as' => 'admin.pimodule.equipmentinspection.store',
        'uses' => 'EquipmentInspectionController@store',
        'middleware' => 'can:pimodule.equipmentinspections.create'
    ]);
    $router->get('equipmentinspections/{equipmentinspection}/edit', [
        'as' => 'admin.pimodule.equipmentinspection.edit',
        'uses' => 'EquipmentInspectionController@edit',
        'middleware' => 'can:pimodule.equipmentinspections.edit'
    ]);
    $router->put('equipmentinspections/{equipmentinspection}', [
        'as' => 'admin.pimodule.equipmentinspection.update',
        'uses' => 'EquipmentInspectionController@update',
        'middleware' => 'can:pimodule.equipmentinspections.edit'
    ]);
    $router->delete('equipmentinspections/{equipmentinspection}', [
        'as' => 'admin.pimodule.equipmentinspection.destroy',
        'uses' => 'EquipmentInspectionController@destroy',
        'middleware' => 'can:pimodule.equipmentinspections.destroy'
    ]);
    $router->post('equipmentinspections/export', [
        'as' => 'admin.pimodule.equipmentinspection.export',
        'uses' => 'EquipmentInspectionController@export',
        // 'middleware' => 'can:pimodule.equipmentinspections.index'
    ]);
    $router->bind('record', function ($id) {
        return app('Modules\Pimodule\Repositories\RecordRepository')->find($id);
    });
    $router->get('records', [
        'as' => 'admin.pimodule.record.index',
        'uses' => 'RecordController@index',
        'middleware' => 'can:pimodule.records.index'
    ]);
    $router->get('records/create', [
        'as' => 'admin.pimodule.record.create',
        'uses' => 'RecordController@create',
        'middleware' => 'can:pimodule.records.create'
    ]);
    $router->post('{equipmentinspection}/records', [
        'as' => 'admin.pimodule.record.store',
        'uses' => 'RecordController@store',
        'middleware' => 'can:pimodule.records.create'
    ]);
    $router->get('records/{record}/edit', [
        'as' => 'admin.pimodule.record.edit',
        'uses' => 'RecordController@edit',
        'middleware' => 'can:pimodule.records.edit'
    ]);
    $router->put('records/{record}', [
        'as' => 'admin.pimodule.record.update',
        'uses' => 'RecordController@update',
        'middleware' => 'can:pimodule.records.edit'
    ]);
    $router->delete('records/{record}', [
        'as' => 'admin.pimodule.record.destroy',
        'uses' => 'RecordController@destroy',
        'middleware' => 'can:pimodule.records.destroy'
    ]);
    $router->bind('recordstatus', function ($id) {
        return app('Modules\Pimodule\Repositories\RecordStatusRepository')->find($id);
    });
    $router->get('recordstatuses', [
        'as' => 'admin.pimodule.recordstatus.index',
        'uses' => 'RecordStatusController@index',
        'middleware' => 'can:pimodule.recordstatuses.index'
    ]);
    $router->get('recordstatuses/create', [
        'as' => 'admin.pimodule.recordstatus.create',
        'uses' => 'RecordStatusController@create',
        'middleware' => 'can:pimodule.recordstatuses.create'
    ]);
    $router->post('recordstatuses', [
        'as' => 'admin.pimodule.recordstatus.store',
        'uses' => 'RecordStatusController@store',
        'middleware' => 'can:pimodule.recordstatuses.create'
    ]);
    $router->get('recordstatuses/{recordstatus}/edit', [
        'as' => 'admin.pimodule.recordstatus.edit',
        'uses' => 'RecordStatusController@edit',
        'middleware' => 'can:pimodule.recordstatuses.edit'
    ]);
    $router->put('recordstatuses/{recordstatus}', [
        'as' => 'admin.pimodule.recordstatus.update',
        'uses' => 'RecordStatusController@update',
        'middleware' => 'can:pimodule.recordstatuses.edit'
    ]);
    $router->delete('recordstatuses/{recordstatus}', [
        'as' => 'admin.pimodule.recordstatus.destroy',
        'uses' => 'RecordStatusController@destroy',
        'middleware' => 'can:pimodule.recordstatuses.destroy'
    ]);
    $router->bind('settings', function ($id) {
        return app('Modules\Pimodule\Repositories\SettingsRepository')->find($id);
    });
    $router->get('settings', [
        'as' => 'admin.pimodule.settings.index',
        'uses' => 'SettingsController@index',
        'middleware' => 'can:pimodule.settings.index'
    ]);
    $router->get('settings/create', [
        'as' => 'admin.pimodule.settings.create',
        'uses' => 'SettingsController@create',
        'middleware' => 'can:pimodule.settings.create'
    ]);
    $router->post('settings', [
        'as' => 'admin.pimodule.settings.store',
        'uses' => 'SettingsController@store',
        'middleware' => 'can:pimodule.settings.create'
    ]);
    $router->get('settings/{settings}/edit', [
        'as' => 'admin.pimodule.settings.edit',
        'uses' => 'SettingsController@edit',
        'middleware' => 'can:pimodule.settings.edit'
    ]);
    $router->put('settings/{settings}', [
        'as' => 'admin.pimodule.settings.update',
        'uses' => 'SettingsController@update',
        'middleware' => 'can:pimodule.settings.edit'
    ]);
    $router->delete('settings/{settings}', [
        'as' => 'admin.pimodule.settings.destroy',
        'uses' => 'SettingsController@destroy',
        'middleware' => 'can:pimodule.settings.destroy'
    ]);
    $router->bind('equipment', function ($id) {
        return app('Modules\Base\Repositories\EquipmentRepository')->find($id);
    });
    $router->get('equipment/', [
        'as' => 'admin.pimodule.equipment.index',
        'uses' => 'EquipmentController@index',
        'middleware' => 'can:pimodule.equipment.index'
    ]);
    $router->get('equipment/create', [
        'as' => 'admin.pimodule.equipment.create',
        'uses' => 'EquipmentController@create',
        'middleware' => 'can:pimodule.equipment.create'
    ]);
    $router->post('equipment', [
        'as' => 'admin.pimodule.equipment.store',
        'uses' => 'EquipmentController@store',
        'middleware' => 'can:pimodule.equipment.create'
    ]);
    $router->get('equipment/edit', [
        'as' => 'admin.pimodule.equipment.edit',
        'uses' => 'EquipmentController@edit',
        'middleware' => 'can:pimodule.equipment.edit'
    ]);
    $router->get('equipment/inspection/', [
        'as' => 'admin.pimodule.equipment.inspections',
        'uses' => 'EquipmentController@inspections',
        'middleware' => 'can:pimodule.equipment.edit'
    ]);
    $router->put('equipment', [
        'as' => 'admin.pimodule.equipment.update',
        'uses' => 'EquipmentController@update',
        'middleware' => 'can:pimodule.equipment.edit'
    ]);
    $router->delete('equipment/{equipment}', [
        'as' => 'admin.pimodule.equipment.destroy',
        'uses' => 'EquipmentController@destroy',
        'middleware' => 'can:pimodule.equipment.destroy'
    ]);
    $router->bind('weekendplan', function ($id) {
        return app('Modules\Pimodule\Repositories\WeekendPlanRepository')->find($id);
    });
    $router->get('weekendplans', [
        'as' => 'admin.pimodule.weekendplan.index',
        'uses' => 'WeekendPlanController@index',
        'middleware' => 'can:pimodule.weekendplans.index'
    ]);
    $router->get('weekendplans/create', [
        'as' => 'admin.pimodule.weekendplan.create',
        'uses' => 'WeekendPlanController@create',
        'middleware' => 'can:pimodule.weekendplans.create'
    ]);
    $router->post('weekendplans', [
        'as' => 'admin.pimodule.weekendplan.store',
        'uses' => 'WeekendPlanController@store',
        'middleware' => 'can:pimodule.weekendplans.create'
    ]);
    $router->get('weekendplans/{weekendplan}/edit', [
        'as' => 'admin.pimodule.weekendplan.edit',
        'uses' => 'WeekendPlanController@edit',
        'middleware' => 'can:pimodule.weekendplans.edit'
    ]);
    $router->put('weekendplans/{weekendplan}', [
        'as' => 'admin.pimodule.weekendplan.update',
        'uses' => 'WeekendPlanController@update',
        'middleware' => 'can:pimodule.weekendplans.edit'
    ]);
    $router->delete('weekendplans/{weekendplan}', [
        'as' => 'admin.pimodule.weekendplan.destroy',
        'uses' => 'WeekendPlanController@destroy',
        'middleware' => 'can:pimodule.weekendplans.destroy'
    ]);
    $router->bind('planrepair', function ($id) {
        return app('Modules\Pimodule\Repositories\PlanRepairRepository')->find($id);
    });
    $router->get('planrepairs', [
        'as' => 'admin.pimodule.planrepair.index',
        'uses' => 'PlanRepairController@index',
        'middleware' => 'can:pimodule.planrepairs.index'
    ]);
    $router->get('planrepairs/{planrepair}/timeline', [
        'as' => 'admin.pimodule.planrepair.timeline',
        'uses' => 'PlanRepairController@timeline',
        'middleware' => 'can:pimodule.planrepairs.index'
    ]);
    $router->get('planrepairs/create', [
        'as' => 'admin.pimodule.planrepair.create',
        'uses' => 'PlanRepairController@create',
        'middleware' => 'can:pimodule.planrepairs.create'
    ]);
    $router->post('planrepairs', [
        'as' => 'admin.pimodule.planrepair.store.free',
        'uses' => 'PlanRepairController@storeFree',
        'middleware' => 'can:pimodule.planrepairs.create'
    ]);
    $router->post('{equipmentinspection}/{record}/planrepairs', [
        'as' => 'admin.pimodule.planrepair.store',
        'uses' => 'PlanRepairController@store',
        'middleware' => 'can:pimodule.planrepairs.create'
    ]);
    $router->get('planrepairs/{planrepair}/edit', [
        'as' => 'admin.pimodule.planrepair.edit',
        'uses' => 'PlanRepairController@edit',
        'middleware' => 'can:pimodule.planrepairs.edit'
    ]);
    $router->put('planrepairs/{planrepair}', [
        'as' => 'admin.pimodule.planrepair.update',
        'uses' => 'PlanRepairController@update',
        'middleware' => 'can:pimodule.planrepairs.edit'
    ]);
    $router->delete('planrepairs/{planrepair}', [
        'as' => 'admin.pimodule.planrepair.destroy',
        'uses' => 'PlanRepairController@destroy',
        'middleware' => 'can:pimodule.planrepairs.destroy'
    ]);
    $router->bind('repairlog', function ($id) {
        return app('Modules\Pimodule\Repositories\RepairLogRepository')->find($id);
    });
    $router->get('repairlogs', [
        'as' => 'admin.pimodule.repairlog.index',
        'uses' => 'RepairLogController@index',
        'middleware' => 'can:pimodule.repairlogs.index'
    ]);
    $router->get('repairlogs/create', [
        'as' => 'admin.pimodule.repairlog.create',
        'uses' => 'RepairLogController@create',
        'middleware' => 'can:pimodule.repairlogs.create'
    ]);
    $router->post('repairlogs/{planrepair}', [
        'as' => 'admin.pimodule.repairlog.store',
        'uses' => 'RepairLogController@store',
        'middleware' => 'can:pimodule.repairlogs.create'
    ]);
    $router->get('repairlogs/{repairlog}/edit', [
        'as' => 'admin.pimodule.repairlog.edit',
        'uses' => 'RepairLogController@edit',
        'middleware' => 'can:pimodule.repairlogs.edit'
    ]);
    $router->put('repairlogs/{repairlog}', [
        'as' => 'admin.pimodule.repairlog.update',
        'uses' => 'RepairLogController@update',
        'middleware' => 'can:pimodule.repairlogs.edit'
    ]);
    $router->delete('repairlogs/{repairlog}', [
        'as' => 'admin.pimodule.repairlog.destroy',
        'uses' => 'RepairLogController@destroy',
        'middleware' => 'can:pimodule.repairlogs.destroy'
    ]);

    $router->bind('patrol', function ($id) {
        return app('Modules\Pimodule\Repositories\PatrolRepository')->find($id);
    });
    $router->get('patrols', [
        'as' => 'admin.pimodule.patrol.index',
        'uses' => 'PatrolController@index',
        'middleware' => 'can:pimodule.patrols.index'
    ]);
    $router->get('patrols/create', [
        'as' => 'admin.pimodule.patrol.create',
        'uses' => 'PatrolController@create',
        'middleware' => 'can:pimodule.patrols.create'
    ]);
    $router->post('patrols', [
        'as' => 'admin.pimodule.patrol.store',
        'uses' => 'PatrolController@store',
        'middleware' => 'can:pimodule.patrols.create'
    ]);
    $router->get('patrols/{patrol}/edit', [
        'as' => 'admin.pimodule.patrol.edit',
        'uses' => 'PatrolController@edit',
        'middleware' => 'can:pimodule.patrols.edit'
    ]);
    $router->put('patrols/{patrol}', [
        'as' => 'admin.pimodule.patrol.update',
        'uses' => 'PatrolController@update',
        'middleware' => 'can:pimodule.patrols.edit'
    ]);
    $router->delete('patrols/{patrol}', [
        'as' => 'admin.pimodule.patrol.destroy',
        'uses' => 'PatrolController@destroy',
        'middleware' => 'can:pimodule.patrols.destroy'
    ]);
// append














});