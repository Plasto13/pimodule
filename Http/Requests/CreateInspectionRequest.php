<?php

namespace Modules\Pimodule\Http\Requests;

use Modules\Core\Internationalisation\BaseFormRequest;

class CreateInspectionRequest extends BaseFormRequest
{
    public function rules()
    {
        return [
            'title' => 'required|unique:pimodule__inspections|max:255',
            'description' => 'max:255',
        ];
    }

    public function translationRules()
    {
        return [];
    }

    public function authorize()
    {
        return true;
    }

    public function messages()
    {
        return [];
    }

    public function translationMessages()
    {
        return [];
    }
}
