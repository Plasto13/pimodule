<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => '/v1/pimodule/planrepair', 'middleware' => 'api.token'], function (Router $router) {
    $router->post('/storno', [
        'as' => 'api.pimodule.equipmentinspections.planrepair.storno',
        'uses' => 'EquipmentInspectionController@storno',
        'middleware' => 'token-can:pimodule.equipmentinspections.edit',
    ]);
    $router->post('/replan', [
        'as' => 'api.pimodule.equipmentinspections.planrepair.replan',
        'uses' => 'PlanRepairController@replan',
        'middleware' => 'token-can:pimodule.equipmentinspections.edit',
    ]);
});
$router->group(['prefix' => '/v1/pimodule/equipment', 'middleware' => 'api.token'], function (Router $router) {
    $router->post('/detail', [
        'as' => 'api.pimodule.equipment.detail',
        'uses' => 'EquipmentController@show',
        'middleware' => 'token-can:pimodule.equipmentinspections.index',
    ]);

});

$router->group(['prefix' => '/v1/pimodule/record'], function (Router $router) {
    $router->post('/{equipmentinspections}/store', [
         'as' => 'api.pimodule.equipmentinspections.record.store',
        'uses' => 'RecordController@store',
    ]);
    
});
