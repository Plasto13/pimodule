<?php

namespace Modules\Pimodule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;
use Modules\Base\Entities\Equipment;
use Modules\Pimodule\Entities\EquipmentInspection;
use Modules\Pimodule\Entities\PlanRepair;
use Modules\Pimodule\Entities\Record;
use Modules\Pimodule\Http\Requests\CreatePlanRepairRequest;
use Modules\Pimodule\Http\Requests\UpdatePlanRepairRequest;
use Modules\Pimodule\Repositories\PlanRepairRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Routing\Controller;
use Modules\User\Entities\Sentinel\User;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Html\Builder;

class PlanRepairGuestController extends Controller
{
        /**
     * @var PlanRepairRepository
     */
    private $planrepair;

    public function __construct(PlanRepairRepository $planrepair)
    {
        $this->planrepair = $planrepair;

        $this->base['route'] = route('guest.pimodule.planrepair.index');
        $this->base['filters'] = [
             [
            'name' => 'ended',
            'type' => 'simple',
            'view' => 'base::filters.simple',
            'label' => trans('pimodule::planrepairs.filter.ended'),
            'currentValue' => null,
            ],
             [
            'name' => 'hall',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::halls.title.halls'),
            'currentValue' => null,
            'values' => Hall::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber halu'
            ],
            [
            'name' => 'line',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::lines.title.lines'),
            'currentValue' => null,
            'values' => Line::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber'
            ],
            [
            'name' => 'equipment',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::equipment.title.equipment'),
            'currentValue' => null,
            'values' => Equipment::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber'
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
 public function index(Builder $builder, Request $request)
    {

        $users = User::select('first_name','last_name','id')
            ->get()->mapWithKeys(function ($item) {
                return [$item['id'] => $item['first_name'].' '.$item['last_name']];
            })->toArray();

        if (request()->ajax()) {
            $datatables =  DataTables::of(PlanRepair::with(['equipmentInspection', 'record'])->whereNull('finished'))
                    ->editColumn('equipment_inspection_id', function (PlanRepair $planrepair) {
                        return $planrepair->equipmentInspection->inspection->title;
                    })
                    ->editColumn('user_id', function (PlanRepair $planrepair) {
                        if (is_object($planrepair->user)) {
                            return $planrepair->user->first_name.' '.$planrepair->user->last_name;
                        }
                    })
                    ->editColumn('ended', function (PlanRepair $planrepair) {
                        return $planrepair->finished ? trans('pimodule::planrepairs.table.yes') :  trans('pimodule::planrepairs.table.no');
                    })
                    ->editColumn('equipment_inspection.equipment_id', function (PlanRepair $planrepair) {
                        return  $planrepair->equipmentInspection->equipment->line->hall->equipment_name.' - '.$planrepair->equipmentInspection->equipment->line->equipment_name.' - '. $planrepair->equipmentInspection->equipment->equipment_name;
                    })
                    ->editColumn('repair_date', function (PlanRepair $planrepair) {
                        if ($planrepair->repair_date) {
                            return $planrepair->repair_date->format('d.m.Y');
                        }
                    })
                    ->filterColumn('repair_date', function ($query, $keyword) {
                        $query->whereRaw("DATE_FORMAT(last_check,'%d/%m/%Y') like ?", ["%$keyword%"]);
                    })
                    ->filter(function ($query) use ($request) {
                        if ($request->get('ended')) {
                           $query->whereNotNull('finished');
                        }
                        if ($name = $request->get('inspection')) {
                            $query->whereHas('equipmentInspection.inspection', function ($q) use ($name) {
                                $q->where('title', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('hall')) {
                            $query->whereHas('equipmentInspection.equipment.line.hall', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('line')) {
                            $query->whereHas('equipmentInspection.equipment.line', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('equipment')) {
                            $query->whereHas('equipmentInspection.equipment', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('user')) {
                            $query->whereHas('user', function ($q) use ($name) {
                                $sql = "CONCAT(users.first_name,' ',users.last_name)  like ?";
                                $q->whereRaw($sql, ["%{$name}%"]);
                            });
                        }
                    })
                    ->editColumn('action', function (PlanRepair $planrepair) use ($users) {
                        return view('pimodule::guest.planrepairs.columns.action')
                                             ->with(['planrepair' => $planrepair, 'users' => $users]);
                    })
                    ->rawColumns(['action', 'record', 'next_check'])
                    ->toJson();
                    return $datatables;
        }

        $html = $builder->columns($this->planrepair->getCollumns())
                        ->parameters([
                                    'paging' => true,
                                    'searching' => true,
                                    'info' => false,
                                    'language' => [
                                        'url' => asset("modules/core/js/vendor/datatables/".locale().".json")
                                        ],
                                    'order' => [
                                            5,
                                            'asc'
                                        ],
                                    ]);
                        
                        $base = $this->base;

        return view('pimodule::guest.planrepairs.index', compact('html','base'));
    }


    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('pimodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pimodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('pimodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

        public function timeline(PlanRepair $planrepair)
    {
        $logs = $planrepair->repairLog()->orderByDesc('created_at')->get();
        return view('pimodule::admin.planrepairs.timeline', compact('logs', 'planrepair'));
    }
}
