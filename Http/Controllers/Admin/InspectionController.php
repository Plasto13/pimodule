<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Excel;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;
use Modules\Base\Entities\Equipment;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Pimodule\Entities\Inspection;
use Modules\Pimodule\Http\Requests\CreateInspectionRequest;
use Modules\Pimodule\Http\Requests\UpdateInspectionRequest;
use Modules\Pimodule\Repositories\InspectionRepository;
use Modules\User\Entities\Sentinel\User;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Html\Builder;

class InspectionController extends AdminBaseController
{
    /**
     * @var InspectionRepository
     */
    private $inspection;

    public function __construct(InspectionRepository $inspection)
    {
        parent::__construct();

        $this->inspection = $inspection;

        $this->base['route'] = route('admin.pimodule.inspection.index');
        $this->base['filters'] = [
             [
            'name' => 'ended',
            'type' => 'simple',
            'view' => 'base::filters.simple',
            'label' => trans('pimodule::planrepairs.filter.ended'),
            'currentValue' => null,
            ],
             [
            'name' => 'hall',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::halls.title.halls'),
            'currentValue' => null,
            'values' => Hall::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber halu'
            ],
            [
            'name' => 'line',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::lines.title.lines'),
            'currentValue' => null,
            'values' => Line::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber'
            ],
            [
            'name' => 'equipment',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::equipment.title.equipment'),
            'currentValue' => null,
            'values' => Equipment::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber'
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder, Request $request)
    {
        // dd($inspections = $this->inspection->all()->first());

        if (request()->ajax()) {
            return DataTables::of(Inspection::with('equipment'))
                        // ->editColumn('equipment.cycle', function (Inspection $inspection) {
                        // if ($inspection->equipment) {
                        //     $first = $inspection->equipment->first;
                        //     return $first->pivot->pivot->cycle;
                        // }
                        // })
                        // ->filter(function ($query) use ($request) {
                        // if ($request->get('title')) {
                        //    $query->where('title', 'like', "%$name%");
                        // }
                        // if ($request->get('description')) {
                        //    $query->where('description', 'like', "%$name%");
                        // }
                        // if ($request->get('cycle')) {
                        //    $query->where('cycle', 'like', "%$name%");
                        // }
                        // if ($name = $request->get('inspection')) {
                        //     $query->whereHas('equipmentInspection.inspection', function ($q) use ($name) {
                        //         $q->where('title', 'like', "%$name%");
                        //     });
                        // }
                        // if ($name = $request->get('hall')) {
                        //     $query->whereHas('equipmentInspection.equipment.line.hall', function ($q) use ($name) {
                        //         $q->where('equipment_name', 'like', "%$name%");
                        //     });
                        // }
                        // if ($name = $request->get('line')) {
                        //     $query->whereHas('equipmentInspection.equipment.line', function ($q) use ($name) {
                        //         $q->where('equipment_name', 'like', "%$name%");
                        //     });
                        // }
                        // if ($name = $request->get('equipment')) {
                        //     $query->whereHas('equipmentInspection.equipment', function ($q) use ($name) {
                        //         $q->where('equipment_name', 'like', "%$name%");
                        //     });
                        // }
                        // if ($name = $request->get('user')) {
                        //     $query->whereHas('user', function ($q) use ($name) {
                        //         $sql = "CONCAT(users.first_name,' ',users.last_name)  like ?";
                        //         $q->whereRaw($sql, ["%{$name}%"]);
                        //     });
                        // }
                    // })                   
                    ->editColumn('action', 'pimodule::admin.inspections.columns.action')
                    ->rawColumns(['action'])
                    ->toJson();
        }
        
        $html = $builder->columns($this->inspection->getCollumns())
                        ->parameters([
                                    'paging' => true,
                                    'searching' => true,
                                    'info' => false,
                                    'language' => [
                                        'url' => asset("modules/core/js/vendor/datatables/".locale().".json")
                                        ],
                                    'order' => [0,'desc'],
                                    ]);

        return view('pimodule::admin.inspections.index', compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $equipments = Equipment::all();
        $user = User::all()->pluck('last_name','id')->toArray();

        return view('pimodule::admin.inspections.create',compact('user','equipments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateInspectionRequest $request
     * @return Response
     */
    public function store(CreateInspectionRequest $request)
    {
        $saved = $this->inspection->create($request->all());
        if (is_array($request->equipment)) {
                $related = array_where($request->equipment , function ($value)
                                    {
                                        if (count($value) == 3) {
                                            return $value;
                                        }
                                    });
                $saved->equipment()->attach($related);
            }

        return redirect()->route('admin.pimodule.inspection.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::inspections.title.inspections')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Inspection $inspection
     * @return Response
     */
    public function edit(Inspection $inspection)
    {
        $equipments = Equipment::all();
        $user = User::all()->pluck('last_name','id')->toArray();
        return view('pimodule::admin.inspections.edit', compact('inspection','user','equipments'));
    }

    public function equipment(Inspection $inspection)
    {
        $user = User::all()->pluck('last_name','id')->toArray();
        $equipment = Equipment::with('line.hall')->get();
        return response()->json($this->inspection->getTable($equipment,'inspection',$user,$inspection->id));
    }

    public function all()
    {
        $user = User::all()->pluck('last_name','id')->toArray();
        $equipment = Equipment::all();
        return response()->json($this->inspection->getTable($equipment,'inspection',$user));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  Inspection $inspection
     * @param  UpdateInspectionRequest $request
     * @return Response
     */
    public function update(Inspection $inspection, UpdateInspectionRequest $request)
    {
        $saved = $this->inspection->update($inspection, $request->all());
        if (is_array($request->equipment)) {
            $related = array_where($request->equipment , function ($value){
                        if (count($value) == 3) {
                            return $value;
                        }
                    });
            $saved->equipment()->sync($related);
        }

        return redirect()->route('admin.pimodule.inspection.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::inspections.title.inspections')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Inspection $inspection
     * @return Response
     */
    public function destroy(Inspection $inspection)
    {
        $this->inspection->destroy($inspection);

        return redirect()->route('admin.pimodule.inspection.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::inspections.title.inspections')]));
    }

    public function import(Request $request)
    {        
        if ($request->hasFile('import')) {
            $inspection = $this->inspection;

            $file = $request->file('import');
            $name =$file->getClientOriginalName();
            $path = $file->storeAs('import/inspection', $name, 'public');
            $xls = storage_path('app/public/'.$path);

            Excel::filter('chunk')->load($xls)->chunk(300, function ($results) use ($inspection) {
                foreach ($results as $row) {
                    if ($row->title) {
                        $inspection->create($row->toArray());
                    }
                }
            });
            
            return redirect()->route('admin.pimodule.inspection.index')
            ->withSuccess(trans('pimodule::inspections.messages.resource import', ['name' => trans('pimodule::equippmnets.title.items')]));
        }
        return redirect()->route('admin.pimodule.inspection.index')
            ->withSuccess(trans('pimodule::equipments.messages.resource import', ['name' => trans('pimodule::equippmnets.title.items')]));
    }
}
