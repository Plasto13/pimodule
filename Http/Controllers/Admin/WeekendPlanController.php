<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pimodule\Entities\WeekendPlan;
use Modules\Pimodule\Http\Requests\CreateWeekendPlanRequest;
use Modules\Pimodule\Http\Requests\UpdateWeekendPlanRequest;
use Modules\Pimodule\Repositories\WeekendPlanRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class WeekendPlanController extends AdminBaseController
{
    /**
     * @var WeekendPlanRepository
     */
    private $weekendplan;

    public function __construct(WeekendPlanRepository $weekendplan)
    {
        parent::__construct();

        $this->weekendplan = $weekendplan;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$weekendplans = $this->weekendplan->all();

        return view('pimodule::admin.weekendplans.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.weekendplans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateWeekendPlanRequest $request
     * @return Response
     */
    public function store(CreateWeekendPlanRequest $request)
    {
        $this->weekendplan->create($request->all());

        return redirect()->route('admin.pimodule.weekendplan.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::weekendplans.title.weekendplans')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  WeekendPlan $weekendplan
     * @return Response
     */
    public function edit(WeekendPlan $weekendplan)
    {
        return view('pimodule::admin.weekendplans.edit', compact('weekendplan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  WeekendPlan $weekendplan
     * @param  UpdateWeekendPlanRequest $request
     * @return Response
     */
    public function update(WeekendPlan $weekendplan, UpdateWeekendPlanRequest $request)
    {
        $this->weekendplan->update($weekendplan, $request->all());

        return redirect()->route('admin.pimodule.weekendplan.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::weekendplans.title.weekendplans')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  WeekendPlan $weekendplan
     * @return Response
     */
    public function destroy(WeekendPlan $weekendplan)
    {
        $this->weekendplan->destroy($weekendplan);

        return redirect()->route('admin.pimodule.weekendplan.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::weekendplans.title.weekendplans')]));
    }
}
