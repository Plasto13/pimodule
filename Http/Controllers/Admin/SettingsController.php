<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pimodule\Entities\Settings;
use Modules\Pimodule\Http\Requests\CreateSettingsRequest;
use Modules\Pimodule\Http\Requests\UpdateSettingsRequest;
use Modules\Pimodule\Repositories\SettingsRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class SettingsController extends AdminBaseController
{
    /**
     * @var SettingsRepository
     */
    private $settings;

    public function __construct(SettingsRepository $settings)
    {
        parent::__construct();

        $this->settings = $settings;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        if($settings = Settings::first()) {
            return redirect()->route('admin.pimodule.settings.edit',$settings->id);
        }
        return redirect()->route('admin.pimodule.settings.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateSettingsRequest $request
     * @return Response
     */
    public function store(CreateSettingsRequest $request)
    {
        $this->settings->create($request->all());

        return redirect()->route('admin.pimodule.equipmentinspection.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::settings.title.settings')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Settings $settings
     * @return Response
     */
    public function edit(Settings $settings)
    {
        return view('pimodule::admin.settings.edit', compact('settings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Settings $settings
     * @param  UpdateSettingsRequest $request
     * @return Response
     */
    public function update(Settings $settings, UpdateSettingsRequest $request)
    {
        $this->settings->update($settings, $request->all());

        return redirect()->route('admin.pimodule.equipmentinspection.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::settings.title.settings')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Settings $settings
     * @return Response
     */
    public function destroy(Settings $settings)
    {
        $this->settings->destroy($settings);

        return redirect()->route('admin.pimodule.settings.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::settings.title.settings')]));
    }
}
