<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pimodule\Entities\PlanRepair;
use Modules\Pimodule\Entities\RepairLog;
use Modules\Pimodule\Http\Requests\CreateRepairLogRequest;
use Modules\Pimodule\Http\Requests\UpdateRepairLogRequest;
use Modules\Pimodule\Repositories\RepairLogRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class RepairLogController extends AdminBaseController
{
    /**
     * @var RepairLogRepository
     */
    private $repairlog;

    public function __construct(RepairLogRepository $repairlog)
    {
        parent::__construct();

        $this->repairlog = $repairlog;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$repairlogs = $this->repairlog->all();

        return view('pimodule::admin.repairlogs.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.repairlogs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRepairLogRequest $request
     * @return Response
     */
    public function store(PlanRepair $planrepair, CreateRepairLogRequest $request)
    {
        $planrepair->repairLog()->create($request->all());
        if ($request->ended) {
            $planrepair->finished = true;
            $planrepair->save();
        }

        return redirect()->route('admin.pimodule.planrepair.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::repairlogs.title.repairlogs')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  RepairLog $repairlog
     * @return Response
     */
    public function edit(RepairLog $repairlog)
    {
        return view('pimodule::admin.repairlogs.edit', compact('repairlog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RepairLog $repairlog
     * @param  UpdateRepairLogRequest $request
     * @return Response
     */
    public function update(RepairLog $repairlog, UpdateRepairLogRequest $request)
    {
        $this->repairlog->update($repairlog, $request->all());

        return redirect()->route('admin.pimodule.repairlog.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::repairlogs.title.repairlogs')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  RepairLog $repairlog
     * @return Response
     */
    public function destroy(RepairLog $repairlog)
    {
        $this->repairlog->destroy($repairlog);

        return redirect()->route('admin.pimodule.repairlog.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::repairlogs.title.repairlogs')]));
    }
}
