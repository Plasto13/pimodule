<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pimodule\Entities\PlanRepairRecord;
use Modules\Pimodule\Http\Requests\CreatePlanRepairRecordRequest;
use Modules\Pimodule\Http\Requests\UpdatePlanRepairRecordRequest;
use Modules\Pimodule\Repositories\PlanRepairRecordRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PlanRepairRecordController extends AdminBaseController
{
    /**
     * @var PlanRepairRecordRepository
     */
    private $planrepairrecord;

    public function __construct(PlanRepairRecordRepository $planrepairrecord)
    {
        parent::__construct();

        $this->planrepairrecord = $planrepairrecord;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$planrepairrecords = $this->planrepairrecord->all();

        return view('pimodule::admin.planrepairrecords.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.planrepairrecords.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePlanRepairRecordRequest $request
     * @return Response
     */
    public function store(CreatePlanRepairRecordRequest $request)
    {
        $this->planrepairrecord->create($request->all());

        return redirect()->route('admin.pimodule.planrepairrecord.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::planrepairrecords.title.planrepairrecords')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  PlanRepairRecord $planrepairrecord
     * @return Response
     */
    public function edit(PlanRepairRecord $planrepairrecord)
    {
        return view('pimodule::admin.planrepairrecords.edit', compact('planrepairrecord'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PlanRepairRecord $planrepairrecord
     * @param  UpdatePlanRepairRecordRequest $request
     * @return Response
     */
    public function update(PlanRepairRecord $planrepairrecord, UpdatePlanRepairRecordRequest $request)
    {
        $this->planrepairrecord->update($planrepairrecord, $request->all());

        return redirect()->route('admin.pimodule.planrepairrecord.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::planrepairrecords.title.planrepairrecords')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PlanRepairRecord $planrepairrecord
     * @return Response
     */
    public function destroy(PlanRepairRecord $planrepairrecord)
    {
        $this->planrepairrecord->destroy($planrepairrecord);

        return redirect()->route('admin.pimodule.planrepairrecord.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::planrepairrecords.title.planrepairrecords')]));
    }
}
