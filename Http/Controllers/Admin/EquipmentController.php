<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\User\Entities\Sentinel\User;
use Modules\Pimodule\Entities\Inspection;
use Modules\Base\Entities\Equipment;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;
use Modules\Base\Http\Requests\CreateEquipmentRequest;
use Modules\Base\Http\Requests\UpdateEquipmentRequest;
use Modules\Base\Repositories\EquipmentRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Html\Builder;

class EquipmentController extends AdminBaseController
{
    /**
     * @var EquipmentRepository
     */
    private $equipment;

    public function __construct(EquipmentRepository $equipment)
    {
        parent::__construct();

        $this->equipment = $equipment;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder)
    {
        $halls = Hall::all()->sortBy('position');

        // if (request()->ajax()) {
        //     return DataTables::of(Equipment::with('line.hall'))                   
        //             // ->editColumn('action', 'pimodule::admin.inspections.columns.action')
        //             ->rawColumns(['action'])
        //             ->toJson();
        // }
        
        // $html = $builder->columns($this->equipment->getCollumnsPimodule())
        //                 ->parameters([
        //                             'paging' => true,
        //                             'searching' => true,
        //                             'info' => false,
        //                             'language' => [
        //                                 'url' => asset("modules/core/js/vendor/datatables/".locale().".json")
        //                                 ],
        //                             'order' => [0,'desc'],
        //                             ]);

        return view('pimodule::admin.equipment.indexn', compact('halls'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.equipment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateEquipmentRequest $request
     * @return Response
     */
    public function store(CreateEquipmentRequest $request)
    {
        // $this->equipment->create($request->all());

        return redirect()->route('admin.pimodule.equipment.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::equipment.title.equipment')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Equipment $equipment
     * @return Response
     */
    public function edit(Request $request)
    {
        if ($request->has('h')) {
            $equipment = Hall::findOrFail($request->h);
            $hall_id = $request->h;
        }
        if ($request->has('l')) {
            $equipment = Line::findOrFail($request->l);
            $line_id = $request->l;
        }
        if ($request->has('e')) {
            $equipment = Equipment::findOrFail($request->e);
            $equipment_id = $request->e;
        }
        // dd($equipment);
        return view('pimodule::admin.equipment.edit', compact('equipment','hall_id', 'line_id', 'equipment_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Equipment $equipment
     * @param  UpdateEquipmentRequest $request
     * @return Response
     */
    public function update(Equipment $equipment, UpdateEquipmentRequest $request)
    {
        // dd($request->all());
        if ($request->hall_id) {
            $equipment = Hall::findOrFail($request->hall_id);
        }
        if ($request->line_id) {
            $equipment = Line::findOrFail($request->line_id);
        }
        if ($request->equipment_id) {
            $equipment = Equipment::findOrFail($request->equipment_id);
        }
        if (is_array($request->inspection)) {
            $related = array_where($request->inspection , function ($value){
                        if (count($value) == 3) {
                            return $value;
                        }
                    });
            $equipment->inspection()->sync($related);
        }
        $this->equipment->update($equipment, $request->all());

        return redirect()->route('admin.pimodule.equipment.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::equipment.title.equipment')]));
    }

    public function inspections(Request $request)
    {
        $users = User::select('first_name','last_name','id')
                    ->get()->mapWithKeys(function ($item) {
                        return [$item['id'] => $item['first_name'].' '.$item['last_name']];
                    })->toArray();
        $inspections = Inspection::all();  
        if ($request->has('h')) {
            $equipment = Hall::findOrFail($request->h);
            return response()->json($this->equipment->getInspection($equipment, $inspections, $user,'hall'));      
        }
        if ($request->has('l')) {
            $equipment = Line::findOrFail($request->l);
            return response()->json($this->equipment->getInspection($equipment, $inspections, $user,'line'));
        }
        if ($request->has('e')) {
            $equipment = Equipment::findOrFail($request->e);
            return response()->json($this->equipment->getInspection($equipment, $inspections, $users,'equipment'));
        }
        
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  Equipment $equipment
     * @return Response
     */
    public function destroy(Equipment $equipment)
    {
        $this->equipment->destroy($equipment);

        return redirect()->route('admin.pimodule.equipment.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::equipment.title.equipment')]));
    }
}
