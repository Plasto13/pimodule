<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pimodule\Entities\RecordStatus;
use Modules\Pimodule\Http\Requests\CreateRecordStatusRequest;
use Modules\Pimodule\Http\Requests\UpdateRecordStatusRequest;
use Modules\Pimodule\Repositories\RecordStatusRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class RecordStatusController extends AdminBaseController
{
    /**
     * @var RecordStatusRepository
     */
    private $recordstatus;

    public function __construct(RecordStatusRepository $recordstatus)
    {
        parent::__construct();

        $this->recordstatus = $recordstatus;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $recordstatuses = $this->recordstatus->all();

        return view('pimodule::admin.recordstatuses.index', compact('recordstatuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.recordstatuses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRecordStatusRequest $request
     * @return Response
     */
    public function store(CreateRecordStatusRequest $request)
    {
        $this->recordstatus->create($request->all());

        return redirect()->route('admin.pimodule.recordstatus.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::recordstatuses.title.recordstatuses')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  RecordStatus $recordstatus
     * @return Response
     */
    public function edit(RecordStatus $recordstatus)
    {
        return view('pimodule::admin.recordstatuses.edit', compact('recordstatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  RecordStatus $recordstatus
     * @param  UpdateRecordStatusRequest $request
     * @return Response
     */
    public function update(RecordStatus $recordstatus, UpdateRecordStatusRequest $request)
    {
        $this->recordstatus->update($recordstatus, $request->all());

        return redirect()->route('admin.pimodule.recordstatus.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::recordstatuses.title.recordstatuses')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  RecordStatus $recordstatus
     * @return Response
     */
    public function destroy(RecordStatus $recordstatus)
    {
        $this->recordstatus->destroy($recordstatus);

        return redirect()->route('admin.pimodule.recordstatus.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::recordstatuses.title.recordstatuses')]));
    }
}
