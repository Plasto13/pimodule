<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Auth;
use Carbon\Carbon;
use Exporter;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Notification\Services\Notification;
use Modules\Base\Entities\Equipment;
use Modules\Pimodule\Entities\EquipmentInspection;
use Modules\Pimodule\Entities\Record;
use Modules\Pimodule\Entities\RecordStatus;
use Modules\Pimodule\Entities\Settings;
use Modules\Pimodule\Http\Requests\CreateEquipmentInspectionRequest;
use Modules\Pimodule\Http\Requests\UpdateEquipmentInspectionRequest;
use Modules\Pimodule\Repositories\EquipmentInspectionRepository;
use Modules\Pimodule\Serialiser\EquipmentInspectionSerialiser;
use Modules\User\Entities\Sentinel\User;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Html\Builder;
use Modules\Media\Image\Imagy;

class EquipmentInspectionController extends AdminBaseController
{
    /**
     * @var EquipmentInspectionRepository
     */
    private $equipmentinspection;

    /**
     * Settings class
     */
    private $settings;

    private $base;

    private $notification;

    private $imagy;

    public function __construct(EquipmentInspectionRepository $equipmentinspection, Notification $notification, Imagy $imagy)
    {

        parent::__construct();

        $this->imagy = $imagy;


        $this->equipmentinspection = $equipmentinspection;

        $this->notification = $notification;

        $this->settings = Settings::first();

        $this->base['route'] = route('admin.pimodule.equipmentinspection.index');
        $this->base['filters'] = [
                        [
                        'name' => 'planrepair',
                        'type' => 'simple',
                        'view' => 'base::filters.simple',
                        'label' => trans('pimodule::planrepairs.filter.planrepairs'),
                        'currentValue' => null,
                        ],
                        [
                        'name' => 'planed',
                        'type' => 'simple',
                        'view' => 'base::filters.simple',
                        'label' => trans('pimodule::planrepairs.filter.planed'),
                        'currentValue' => null,
                        ],
                        [
                        'name' => 'hall',
                        'type' => 'select2',
                        'view' => 'base::filters.select2',
                        'label' => trans('base::halls.title.halls'),
                        'currentValue' => null,
                        'values' => Hall::pluck('equipment_name','equipment_name')->unique()->toArray(),
                        'placeholder' => 'Vyber halu'
                        ],
                        [
                        'name' => 'line',
                        'type' => 'select2',
                        'view' => 'base::filters.select2',
                        'label' => trans('base::lines.title.lines'),
                        'currentValue' => null,
                        'values' => Line::pluck('equipment_name','equipment_name')->unique()->toArray(),
                        'placeholder' => 'Vyber'
                        ],
                        [
                        'name' => 'equipment',
                        'type' => 'select2',
                        'view' => 'base::filters.select2',
                        'label' => trans('base::equipment.title.equipment'),
                        'currentValue' => null,
                        'values' => Equipment::pluck('equipment_name','equipment_name')->unique()->toArray(),
                        'placeholder' => 'Vyber'
                        ],
                        [
                        'name' => 'inspection',
                        'type' => 'text',
                        'view' => 'base::filters.text',
                        'label' => 'Inspection',
                        'currentValue' => null,
                        ],
                        [
                        'name' => 'user',
                        'type' => 'select2',
                        'view' => 'base::filters.select2',
                        'label' => 'User',
                        'currentValue' => null,
                        'values' => User::select(DB::raw("CONCAT(users.first_name,' ',users.last_name)  AS name"),'id')->pluck('name', 'name')->toArray(),
                        'placeholder' => 'Vyber'
                        ]                     
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder, Request $request)
    {
        $settings = $this->settings;
        $imagy = $this->imagy;
        $recordstatus = RecordStatus::pluck('title', 'id');
        $users = User::select('first_name','last_name','id')
                    ->get()->mapWithKeys(function ($item) {
                        return [$item['id'] => $item['first_name'].' '.$item['last_name']];
                    })->toArray();

        if (request()->ajax()) {
            return  DataTables::of(EquipmentInspection::with(['equipment.line.hall', 'line', 'hall', 'inspection.files','user','record','planRepair']))
                    // ->orderColumn('next_check','-:next_check $1')
                    ->editColumn('user.last_name', function (EquipmentInspection $equipmentinspection) {
                        if (is_object($equipmentinspection->user)) {
                            return $equipmentinspection->user->first_name.' '.$equipmentinspection->user->last_name;
                        }
                    })
                    ->editColumn('inspection.files', function (EquipmentInspection $equipmentinspection) use($imagy)
                    {
                        if (is_object($inspection = $equipmentinspection->inspection)) {
                            if (isset($inspection->instruction->path)) {
                                $thumb = $imagy->getThumbnail($inspection->instruction->path, 'smallThumb');
                                    return "<a href='{$inspection->instruction->path}' target='_blank'> <img src='$thumb' alt='inspection_instruction' data-toggle='modal' data-target='#photoModal' data-action-target='{$inspection->instruction->path}' /></a>";
                            }
                        }
                    })
                    ->editColumn('equipment.line.hall.equipment_name', function (EquipmentInspection $equipmentinspection) {
                        if (is_object($equipmentinspection->hall)) {
                            return $equipmentinspection->hall->equipment_name;
                        }
                        if (is_object($equipmentinspection->line)) {
                            return $equipmentinspection->line->hall->equipment_name;
                        }
                        return $equipmentinspection->equipment->line->hall->equipment_name;

                    })
                    ->editColumn('equipment.line.equipment_name', function (EquipmentInspection $equipmentinspection) {
                        if (is_object($equipmentinspection->line)) {
                            return $equipmentinspection->line->equipment_name;
                        }
                        if (is_object($equipmentinspection->equipment)) {
                            return $equipmentinspection->equipment->line->equipment_name;
                        }
                        return null;
                    })
                    ->editColumn('equipment.equipment_name', function (EquipmentInspection $equipmentinspection) {
                        if ($equipmentinspection->equipment) {
                            return $equipmentinspection->equipment->equipment_name;
                        }
                        return null;
                    })
                    ->editColumn('record', function (EquipmentInspection $equipmentinspection) {
                        $id = $equipmentinspection->record()->max('id');
                        $record = Record::find($id);
                        
                        if (isset($record->plan_repair) && !isset($record->planRepair)) {
                           return "<span class='fa fa-exclamation label bg-red'> ".trans('pimodule::records.table.plan_repair')."</span>";
                        }
                        if (isset($record->planRepair->finished) && !isset($record->planRepair)) {
                           return "<span class='fa fa-thumbs-o-up label bg-green'> ".trans('pimodule::records.table.repaired')."</span>";
                        }
                        if (!isset($record->planRepair->finished) && isset($record->plan_repair)) {
                           return "<span class='fa fa-wrench label bg-yellow'> ".trans('pimodule::records.table.planned')."</span>";
                        }
                    })
                    ->editColumn('status', function (EquipmentInspection $equipmentinspection){
                        $record = $equipmentinspection->record()->first();
                        $status = RecordStatus::find($record['record_status_id']);
                        return $status['title'];
                    })
                    ->editColumn('last_check', function (EquipmentInspection $equipmentinspection) {
                        if ($equipmentinspection->last_check) {
                            return $equipmentinspection->last_check->format('d.m.Y');
                        }
                    })
                    ->editColumn('next_check', function (EquipmentInspection $equipmentinspection) use($settings) {
                        if ($equipmentinspection->next_check->lt(Carbon::now())) {
                            return "<span class='alert-error label'>".$equipmentinspection->next_check->format('d.m.Y')."</span>";
                        }
                        elseif (isset($settings->prewarning_days) && $equipmentinspection->next_check->subDay($settings->prewarning_days)->lt(Carbon::now())) {
                            return "<span class='alert-warning label'>".$equipmentinspection->next_check->format('d.m.Y')."</span>";
                        }
                        if ($equipmentinspection->next_check) {
                            return $equipmentinspection->next_check->format('d.m.Y');
                        }
                    })
                    ->filterColumn('last_check', function ($query, $keyword) {
                        $query->whereRaw("DATE_FORMAT(last_check,'%d/%m/%Y') like ?", ["%$keyword%"]);
                    })
                    ->filter(function ($query) use ($request) {
                        if ($request->get('planrepair')) {
                           $query->whereHas('record', function($q){
                                $q->whereNotNull('plan_repair')->where('planed');
                            });
                        }
                        if ($request->get('planed')) {
                           $query->whereHas('record', function($q){
                                $q->whereNotNull('planed')->where('finished');
                            });
                        }
                        if ($name = $request->get('inspection')) {
                           $query->whereHas('inspection', function($q) use($name){
                                $q->where('title', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('hall')) {
                           $query->whereHas('equipment.line.hall', function($q) use($name){
                                $q->where('equipment_name', 'like', "%$name%");
                            })->orWhereHas('line.hall', function($q) use($name){
                                $q->where('equipment_name', 'like', "%$name%");
                            })->orWhereHas('hall', function($q) use($name){
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('line')) {
                           $query->whereHas('equipment.line', function($q) use($name){
                                $q->where('equipment_name', 'like', "%$name%");
                            })->orWhereHas('line', function($q) use($name){
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('equipment')) {
                           $query->whereHas('equipment', function($q) use($name){
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }

                        if ($name = $request->get('user')) {
                           $query->whereHas('user', function($q) use($name){
                            $this->base['filters']['user'] = ['currentValue' => $name];
                            $sql = "CONCAT(users.first_name,' ',users.last_name)  like ?";
                                $q->whereRaw($sql, ["%{$name}%"]);
                            });
                        }
                    })
                    ->editColumn('action', function (EquipmentInspection $equipmentinspection) use ($recordstatus, $users) {
                        return view('pimodule::admin.equipmentinspections.columns.action')
                                             ->with(['equipmentinspection'=>$equipmentinspection,'recordstatus'=>$recordstatus, 'users'=> $users]);
                    })
                    ->rawColumns(['action', 'record', 'next_check','inspection.files','description'])
                    ->toJson();
        }

        $html = $builder->columns($this->equipmentinspection->getCollumns())
                        ->parameters([
                                    'paging' => true,
                                    'searching' => false,
                                    'info' => false,
                                    'language' => [
                                        'url' => asset("modules/core/js/vendor/datatables/".locale().".json")
                                        ],
                                    'order' => [
                                            9, // here is the column number
                                            'asc'
                                        ],
                                    ]);
                        
                        $base = $this->base;
        return view('pimodule::admin.equipmentinspections.index', compact('html', 'base', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.equipmentinspections.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateEquipmentInspectionRequest $request
     * @return Response
     */
    public function store(CreateEquipmentInspectionRequest $request)
    {
        $this->equipmentinspection->create($request->all());

        return redirect()->route('admin.pimodule.equipmentinspection.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::equipmentinspections.title.equipmentinspections')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  EquipmentInspection $equipmentinspection
     * @return Response
     */
    public function edit(EquipmentInspection $equipmentinspection)
    {
        return view('pimodule::admin.equipmentinspections.edit', compact('equipmentinspection'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EquipmentInspection $equipmentinspection
     * @param  UpdateEquipmentInspectionRequest $request
     * @return Response
     */
    public function update(EquipmentInspection $equipmentinspection, UpdateEquipmentInspectionRequest $request)
    {
        $this->equipmentinspection->update($equipmentinspection, $request->all());

        return redirect()->route('admin.pimodule.equipmentinspection.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::equipmentinspections.title.equipmentinspections')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  EquipmentInspection $equipmentinspection
     * @return Response
     */
    public function destroy(EquipmentInspection $equipmentinspection)
    {
        $this->equipmentinspection->destroy($equipmentinspection);

        return redirect()->route('admin.pimodule.equipmentinspection.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::equipmentinspections.title.equipmentinspections')]));
    }

    public function export(Request $request, EquipmentInspectionSerialiser $serialiser)
    {
        // dd(Carbon::createFromFormat('d.m.Y',$request->from));
        $file = 'export/pimodule/export-'.Carbon::now()->format('ymd-Hi').'.xlsx';
        $query = DB::table('pimodule__equipment_inspection')
                    ->select('equipment_id', 'inspection_id', 'user_id', 'user_name', 'last_check', 'next_check')->orderBy('equipment_id');

        if ($request->to) {
            $query = $query->where('next_check', '<=', Carbon::createFromFormat('d.m.Y', $request->to));
        }
        if ($request->user_id) {
            $query = $query->where('user_id', '=', $request->user_id);
        }
        $headers = array(
              'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            );
        // dd($query);
        $excel = Exporter::make('Excel');
        $excel->loadQuery($query);
        $excel->setChunk(1000);
        $excel->setSerialiser($serialiser);
        $excel->save($file);
        $response = response()->download($file, 'export-'.Carbon::now()->format('ymd-Hi').'.xlsx', $headers);
        ob_end_clean();
        return $response;
        // $this->notification->to(Auth::user()->id)->push('Export complete', 'Download Here', 'fa fa-hand-peace-o text-green', url($file));

        // return redirect()->back();
    }
}
