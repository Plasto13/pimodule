<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pimodule\Entities\PreventiveInspectionCheck;
use Modules\Pimodule\Http\Requests\CreatePreventiveInspectionCheckRequest;
use Modules\Pimodule\Http\Requests\UpdatePreventiveInspectionCheckRequest;
use Modules\Pimodule\Repositories\PreventiveInspectionCheckRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PreventiveInspectionCheckController extends AdminBaseController
{
    /**
     * @var PreventiveInspectionCheckRepository
     */
    private $preventiveinspectioncheck;

    public function __construct(PreventiveInspectionCheckRepository $preventiveinspectioncheck)
    {
        parent::__construct();

        $this->preventiveinspectioncheck = $preventiveinspectioncheck;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$preventiveinspectionchecks = $this->preventiveinspectioncheck->all();

        return view('pimodule::admin.preventiveinspectionchecks.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.preventiveinspectionchecks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePreventiveInspectionCheckRequest $request
     * @return Response
     */
    public function store(CreatePreventiveInspectionCheckRequest $request)
    {
        $this->preventiveinspectioncheck->create($request->all());

        return redirect()->route('admin.pimodule.preventiveinspectioncheck.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::preventiveinspectionchecks.title.preventiveinspectionchecks')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  PreventiveInspectionCheck $preventiveinspectioncheck
     * @return Response
     */
    public function edit(PreventiveInspectionCheck $preventiveinspectioncheck)
    {
        return view('pimodule::admin.preventiveinspectionchecks.edit', compact('preventiveinspectioncheck'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  PreventiveInspectionCheck $preventiveinspectioncheck
     * @param  UpdatePreventiveInspectionCheckRequest $request
     * @return Response
     */
    public function update(PreventiveInspectionCheck $preventiveinspectioncheck, UpdatePreventiveInspectionCheckRequest $request)
    {
        $this->preventiveinspectioncheck->update($preventiveinspectioncheck, $request->all());

        return redirect()->route('admin.pimodule.preventiveinspectioncheck.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::preventiveinspectionchecks.title.preventiveinspectionchecks')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  PreventiveInspectionCheck $preventiveinspectioncheck
     * @return Response
     */
    public function destroy(PreventiveInspectionCheck $preventiveinspectioncheck)
    {
        $this->preventiveinspectioncheck->destroy($preventiveinspectioncheck);

        return redirect()->route('admin.pimodule.preventiveinspectioncheck.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::preventiveinspectionchecks.title.preventiveinspectionchecks')]));
    }
}
