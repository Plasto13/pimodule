<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Pimodule\Entities\Patrol;
use Modules\Pimodule\Http\Requests\CreatePatrolRequest;
use Modules\Pimodule\Http\Requests\UpdatePatrolRequest;
use Modules\Pimodule\Repositories\PatrolRepository;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;

class PatrolController extends AdminBaseController
{
    /**
     * @var PatrolRepository
     */
    private $patrol;

    public function __construct(PatrolRepository $patrol)
    {
        parent::__construct();

        $this->patrol = $patrol;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //$patrols = $this->patrol->all();

        return view('pimodule::admin.patrols.index', compact(''));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.patrols.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreatePatrolRequest $request
     * @return Response
     */
    public function store(CreatePatrolRequest $request)
    {
        $this->patrol->create($request->all());

        return redirect()->route('admin.pimodule.patrol.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::patrols.title.patrols')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Patrol $patrol
     * @return Response
     */
    public function edit(Patrol $patrol)
    {
        return view('pimodule::admin.patrols.edit', compact('patrol'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Patrol $patrol
     * @param  UpdatePatrolRequest $request
     * @return Response
     */
    public function update(Patrol $patrol, UpdatePatrolRequest $request)
    {
        $this->patrol->update($patrol, $request->all());

        return redirect()->route('admin.pimodule.patrol.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::patrols.title.patrols')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Patrol $patrol
     * @return Response
     */
    public function destroy(Patrol $patrol)
    {
        $this->patrol->destroy($patrol);

        return redirect()->route('admin.pimodule.patrol.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::patrols.title.patrols')]));
    }
}
