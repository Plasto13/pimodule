<?php

namespace Modules\Pimodule\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Pimodule\Entities\EquipmentInspection;
use Modules\Pimodule\Entities\Record;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;
use Modules\Base\Entities\Equipment;
use Modules\Pimodule\Entities\Inspection;
use Modules\Pimodule\Http\Requests\CreateRecordRequest;
use Modules\Pimodule\Http\Requests\UpdateRecordRequest;
use Modules\Pimodule\Repositories\RecordRepository;
use Modules\User\Entities\Sentinel\User;
use Modules\Pimodule\Entities\RecordStatus;
use Yajra\DataTables\DataTables;
use Yajra\DataTables\Html\Builder;

class RecordController extends AdminBaseController
{
    /**
     * @var RecordRepository
     */
    private $record;

    public function __construct(RecordRepository $record)
    {
        parent::__construct();

        $this->record = $record;

        $this->base['route'] = route('admin.pimodule.record.index');
        
        $this->base['filters'] = [
             [
            'name' => 'planrepair',
            'type' => 'simple',
            'view' => 'base::filters.simple',
            'label' => trans('pimodule::planrepairs.filter.planrepairs'),
            'currentValue' => null,
            ],
             [
            'name' => 'hall',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::halls.title.halls'),
            'currentValue' => null,
            'values' => Hall::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber halu'
            ],
            [
            'name' => 'line',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::lines.title.lines'),
            'currentValue' => null,
            'values' => Line::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber'
            ],
            [
            'name' => 'equipment',
            'type' => 'select2',
            'view' => 'base::filters.select2',
            'label' => trans('base::equipment.title.equipment'),
            'currentValue' => null,
            'values' => Equipment::pluck('equipment_name','equipment_name')->unique()->toArray(),
            'placeholder' => 'Vyber'
            ],
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Builder $builder, Request $request)
    {
        // $e = Record::with('files')->find(370);
        // dd($e);

        if (request()->ajax()) {
            return DataTables::of(Record::query()->with(['status','equipmentInspection.equipment.line.hall','user','files']))
                    ->editColumn('photo', 'pimodule::columns.image')
                    // ->editColumn('photo', function (Record $record) {
                    //     return view('pimodule::columns.image')->with(['record' => $record]);
                    // })
                    ->editColumn('equipment', function(Record $record){
                        if ($equipment = $record->equipmentInspection->equipment) {
                            return $equipment->line->hall->equipment_name.' - '.$equipment->line->equipment_name.' - '.$equipment->equipment_name;
                        }
                        return $record->equipmentInspection->equipment->equipment_name;
                    })
                    ->editColumn('inspection', function(Record $record){
                        if ($inspection = $record->equipmentInspection->inspection) {
                            return $inspection->title;
                        }
                        return $record->equipmentInspection->equipment->equipment_name;
                    })
                    ->editColumn('record_status_id', function (Record $record) {
                        return view('pimodule::admin.records.columns.record_status_id')->with(['record' => $record]);
                    })
                    ->editColumn('user.last_name', function (Record $record) {
                        if (is_object($record->user)) {
                            return $record->user->first_name.' '.$record->user->last_name;
                        }
                    })
                    ->editColumn('plan_repair', 'pimodule::admin.records.columns.plan_repair')
                    ->editColumn('action', 'pimodule::admin.records.columns.action')
                    ->filter(function ($query) use ($request) {
                        if ($request->get('planrepair')) {
                           $query->whereNotNull('plan_repair');
                        }
                        if ($name = $request->get('inspection')) {
                            $query->whereHas('equipmentInspection.inspection', function ($q) use ($name) {
                                $q->where('title', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('hall')) {
                            $query->whereHas('equipmentInspection.equipment.line.hall', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('line')) {
                            $query->whereHas('equipmentInspection.equipment.line', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('equipment')) {
                            $query->whereHas('equipmentInspection.equipment', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('user')) {
                            $query->whereHas('user', function ($q) use ($name) {
                                $sql = "CONCAT(users.first_name,' ',users.last_name)  like ?";
                                $q->whereRaw($sql, ["%{$name}%"]);
                            });
                        }
                    })
                    ->rawColumns(['action','record_status_id','description','photo'])
                    ->toJson();
        }

        $html = $builder->columns($this->record->getCollumns())
                        ->parameters([
                                    'paging' => true,
                                    'searching' => true,
                                    'info' => false,
                                    // 'rowReorder' => [
                                    //     'dataSrc' => 'created_at',
                                    // ],
                                    'order' => [
                                            0, // here is the column number
                                            'desc'
                                        ],
                                    'language' => [
                                        'url' => asset("modules/core/js/vendor/datatables/".locale().".json")
                                        ],
                                    ]);
        $base = $this->base;
        return view('pimodule::admin.records.index', compact('html','base'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('pimodule::admin.records.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CreateRecordRequest $request
     * @return Response
     */
    public function store(EquipmentInspection $equipmentinspection, CreateRecordRequest $request)
    {
        $saved = $equipmentinspection->record()->create($request->all());

        $equipmentinspection->last_check = $saved->created_at ;
        $equipmentinspection->next_check = $saved->created_at->addDays($equipmentinspection->cycle);
        $equipmentinspection->update();

        return redirect()->route('admin.pimodule.equipmentinspection.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::records.title.records')]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Record $record
     * @return Response
     */
    public function edit(Record $record)
    {
         $recordstatus = RecordStatus::pluck('title', 'id');
        $users = User::select('first_name','last_name','id')
                    ->get()->mapWithKeys(function ($item) {
                        return [$item['id'] => $item['first_name'].' '.$item['last_name']];
                    })->toArray();
        return view('pimodule::admin.records.edit', compact('record', 'recordstatus', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Record $record
     * @param  UpdateRecordRequest $request
     * @return Response
     */
    public function update(Record $record, UpdateRecordRequest $request)
    {
        $this->record->update($record, $request->all());

        return redirect()->route('admin.pimodule.record.index')
            ->withSuccess(trans('core::core.messages.resource updated', ['name' => trans('pimodule::records.title.records')]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Record $record
     * @return Response
     */
    public function destroy(Record $record)
    {
        $this->record->destroy($record);

        return redirect()->route('admin.pimodule.record.index')
            ->withSuccess(trans('core::core.messages.resource deleted', ['name' => trans('pimodule::records.title.records')]));
    }
}
