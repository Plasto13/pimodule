<?php

namespace Modules\Pimodule\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Routing\Controller;
use Modules\Base\Entities\Equipment;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;
use Modules\Pimodule\Entities\PlanRepair;

class PlanRepairController extends Controller
{

    public function replan(Request $request)
    {
        // return response($request->all(), $status = 200);
        $planrepair = PlanRepair::find($request->planrepair);
        if(is_object($planrepair)){
            $planrepair->repair_date = $request->repair_date;
            $planrepair->update();
            return response(['title'=>'Zaznam upraveny', 'text'=>'Oprava preplanovana na: '.$planrepair->repair_date->format('j.n.Y'), 'type'=>'success'], $status = 200);
        }
        return response($planrepair, $status = 200);
        
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pimodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
