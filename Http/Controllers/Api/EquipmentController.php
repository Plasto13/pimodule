<?php

namespace Modules\Pimodule\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Base\Entities\Equipment;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Request $request)
    {
        return reponse();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('pimodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show(Request $request)
    {
        if ($request->model == 'hall') {
            $model = Hall::findOrFail($request->id);
        }
        if ($request->model == 'line') {
            $model = Line::findOrFail($request->id);
        }
        if ($request->model == 'equipment') {
            $model = Equipment::findOrFail($request->id);
            $planrepairs = $model->planRepair()->get();
            $records = $model->record()->limit(5)->orderBy('created_at')->get();
        }


        return Response::view('pimodule::admin.equipment.partials.detail',compact('model', 'planrepairs','records'));

    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('pimodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function printLabel(Equipment $equipment, Request $request)
    {
        $html = "
        ^XA
        ^FO20,20^BQ,2,10^FDQA,{$equipment->api_key}^FS
        ^XZ
        ";

        # code...
    }
}
