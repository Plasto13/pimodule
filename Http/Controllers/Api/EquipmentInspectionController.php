<?php

namespace Modules\Pimodule\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Modules\Pimodule\Repositories\RecordRepository;

class EquipmentInspectionController extends Controller
{
    /**
    *  @var EquipmentInspectionRepository
    */
    protected $record;

    public function __construct(RecordRepository $record)
    {
        $this->record = $record;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('pimodule::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('pimodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pimodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('pimodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }

    public function storno(Request $request)
    {
        $model = $this->record->find($request->id);
        if (!$model) {
            return Response::json(['errors' => true,
                'message' => trans('pimodule::equipmentinspections.messages.planrepair storno nok')
                ]);
        }
        $this->record->update($model,['plan_repair' => null]);
        return Response::json(['errors' => false,
                            'button' => true,
                            'message' => trans('pimodule::equipmentinspections.messages.planrepair storno ok')
                            ]);
    }
}
