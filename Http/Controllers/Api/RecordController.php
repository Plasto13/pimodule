<?php

namespace Modules\Pimodule\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Pimodule\Entities\EquipmentInspection;
use Modules\Pimodule\Repositories\RecordRepository;

class RecordController extends Controller
{
    /**
    * @var RecordRepository
    */  
    private $record;

    public function __construct(RecordRepository $record)
    {
        $this->record = $record;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('pimodule::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('pimodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(EquipmentInspection $equipmentinspections, Request $request)
    {
        // return response($equipmentinspections);
        
        if ($equipmentinspections) {
            $saved = $this->record->create($request->all());
            $equipmentinspections->record()->save($saved);
            // $equipmentinspections->record()->add($saved);
            $equipmentinspections->last_check = $saved->created_at ;
            $equipmentinspections->next_check = $saved->created_at->addDays($equipmentinspections->cycle);
            $equipmentinspections->update();
            return response(['title'=>'Zaznam uložený', 'text'=>'', 'type'=>'success'], $status = 200);
        }
        return response(['title'=>'Niekde sa stala Chyba!!', 'text'=>'Kontaktujte správcu systému!', 'type'=>'error'], $status = 200);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pimodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('pimodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
