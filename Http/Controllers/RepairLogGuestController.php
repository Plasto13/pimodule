<?php

namespace Modules\Pimodule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Pimodule\Entities\PlanRepair;
use Modules\Pimodule\Entities\RepairLog;
use Modules\Pimodule\Http\Requests\CreateRepairLogRequest;
use Modules\Pimodule\Http\Requests\UpdateRepairLogRequest;
use Modules\Pimodule\Repositories\RepairLogRepository;

class RepairLogGuestController extends Controller
{
    /**
     * @var RepairLogRepository
     */
    private $repairlog;

    public function __construct(RepairLogRepository $repairlog)
    {
        $this->repairlog = $repairlog;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('pimodule::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('pimodule::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(PlanRepair $planrepair, CreateRepairLogRequest $request)
    {
        $planrepair->repairLog()->create($request->all());
        if ($request->ended) {
            $planrepair->finished = true;
            $planrepair->save();
        }

        return redirect()->route('guest.pimodule.planrepair.index')
            ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::repairlogs.title.repairlogs')]));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pimodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('pimodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
