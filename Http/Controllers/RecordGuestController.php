<?php

namespace Modules\Pimodule\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Entities\Sentinel\User;
use Modules\Pimodule\Entities\EquipmentInspection;
use Modules\Pimodule\Entities\Inspection;
use Modules\Base\Entities\Equipment;
use Modules\Pimodule\Entities\Record;
use Modules\Pimodule\Entities\RecordStatus;
use Modules\Pimodule\Repositories\RecordRepository;
use Yajra\DataTables\Datatables;
use Yajra\DataTables\Html\Builder;


class RecordGuestController extends Controller
{
        /**
     * @var RecordRepository
     */
    private $record;

    public function __construct(RecordRepository $record)
    {
        $this->record = $record;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index(Builder $builder, Request $request)
    {
         if (request()->ajax()) {
            return DataTables::of(Record::query()->with(['status','equipmentInspection','user']))
                    ->editColumn('equipment', function(Record $record){
                        if ($equipment = $record->equipmentInspection->equipment) {
                            return $equipment->line->hall->equipment_name.' - '.$equipment->line->equipment_name.' - '.$equipment->equipment_name;
                        }
                        return $record->equipmentInspection->equipment->equipment_name;
                    })
                    ->editColumn('inspection', function(Record $record){
                        if ($inspection = $record->equipmentInspection->inspection) {
                            return $inspection->title;
                        }
                        return $record->equipmentInspection->equipment->equipment_name;
                    })
                    ->editColumn('record_status_id', function (Record $record) {
                        return view('pimodule::admin.records.columns.record_status_id')->with(['record' => $record]);
                    })
                    ->editColumn('user.last_name', function (Record $record) {
                        if (is_object($record->user)) {
                            return $record->user->first_name.' '.$record->user->last_name;
                        }
                    })
                    ->editColumn('plan_repair', 'pimodule::admin.records.columns.plan_repair')
                    ->editColumn('action', 'pimodule::admin.records.columns.action')
                    ->filter(function ($query) use ($request) {
                        if ($name = $request->get('inspection')) {
                            $query->whereHas('inspection', function ($q) use ($name) {
                                $q->where('title', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('hall')) {
                            $query->whereHas('equipment.line.hall', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            })->orWhereHas('line.hall', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            })->orWhereHas('hall', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('line')) {
                            $query->whereHas('equipment.line', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            })->orWhereHas('line', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('equipment')) {
                            $query->whereHas('equipment', function ($q) use ($name) {
                                $q->where('equipment_name', 'like', "%$name%");
                            });
                        }
                        if ($name = $request->get('user')) {
                            $query->whereHas('user', function ($q) use ($name) {
                                $sql = "CONCAT(users.first_name,' ',users.last_name)  like ?";
                                $q->whereRaw($sql, ["%{$name}%"]);
                            });
                        }
                    })
                    ->rawColumns(['action'])
                    ->toJson();
        }

        $html = $builder->columns($this->record->getCollumns())
                        ->parameters([
                                    'paging' => true,
                                    'searching' => true,
                                    'info' => false,
                                    'rowReorder' => [
                                        'dataSrc' => 'created_at',
                                    ],
                                    'language' => [
                                        'url' => asset("modules/core/js/vendor/datatables/".locale().".json")
                                        ],
                                    ]);
        return view('pimodule::guest.records.index', compact('html'));
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(EquipmentInspection $equipmentinspection)
    {
        $recordStatus = RecordStatus::all()->pluck('title', 'id')->toArray();
        $users = User::select('first_name','last_name','id')
                    ->get()->mapWithKeys(function ($item) {
                        return [$item['id'] => $item['first_name'].' '.$item['last_name']];
                    })->toArray();
        
        return view('pimodule::guest.records.create', compact('equipmentinspection', 'recordStatus', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(EquipmentInspection $equipmentinspection, Request $request)
    {
        $saved = $equipmentinspection->record()->create($request->all());

        $equipmentinspection->last_check = $saved->created_at ;
        $equipmentinspection->next_check = $saved->created_at->addDays($equipmentinspection->cycle);
        $equipmentinspection->update();
       
        return redirect()->route('guest.pimodule.equipmentInspection.index')
        ->withSuccess(trans('core::core.messages.resource created', ['name' => trans('pimodule::equipmentinspections.title.equipmentinspections')]));
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('pimodule::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('pimodule::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
