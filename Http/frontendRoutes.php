<?php

use Illuminate\Routing\Router;

$router->group(['prefix' =>'/pimodule'], function (Router $router) {
    $router->bind('equipmentinspection', function ($id) {
        return app('Modules\Pimodule\Repositories\EquipmentInspectionRepository')->find($id);
    });
    $router->post('equipmentinspections/export', [
        'as' => 'guest.pimodule.equipmentinspection.export',
        'uses' => 'EquipmentInspectionGuestController@export',
    ]);
    $router->get('equipmentinspections', [
        'as' => 'guest.pimodule.equipmentInspection.index',
        'uses' => 'EquipmentInspectionGuestController@index',
    ]);

    $router->bind('record', function ($id) {
        return app('Modules\Pimodule\Repositories\RecordRepository')->find($id);
    });
    $router->get('records', [
        'as' => 'record.guest.item.index',
        'uses' => 'RecordGuestController@index',
    ]);
    $router->get('{equipmentinspection}/records/create', [
        'as' => 'guest.pimodule.record.create',
        'uses' => 'RecordGuestController@create',
    ]);
    $router->post('{equipmentinspection}/records', [
        'as' => 'guest.pimodule.record.store',
        'uses' => 'RecordGuestController@store',
    ]);

    $router->bind('planrepair', function ($id) {
        return app('Modules\Pimodule\Repositories\PlanRepairRepository')->find($id);
    });
    $router->get('planrepairs', [
        'as' => 'guest.pimodule.planrepair.index',
        'uses' => 'PlanRepairGuestController@index',
    ]);
    $router->get('planrepairs/{planrepair}/timeline', [
        'as' => 'guest.pimodule.planrepair.timeline',
        'uses' => 'PlanRepairGuestController@timeline',
    ]);

    $router->bind('repairlog', function ($id) {
        return app('Modules\Pimodule\Repositories\RepairLogRepository')->find($id);
    });
    $router->post('repairlogs/{planrepair}', [
        'as' => 'guest.pimodule.repairlog.store',
        'uses' => 'RepairLogGuestController@store',
    ]);
});
