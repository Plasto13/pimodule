<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\PreventiveInspectionCheckRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPreventiveInspectionCheckRepository extends EloquentBaseRepository implements PreventiveInspectionCheckRepository
{
}
