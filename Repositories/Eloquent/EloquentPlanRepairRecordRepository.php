<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\PlanRepairRecordRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPlanRepairRecordRepository extends EloquentBaseRepository implements PlanRepairRecordRepository
{
}
