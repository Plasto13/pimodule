<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\RepairLogRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentRepairLogRepository extends EloquentBaseRepository implements RepairLogRepository
{
}
