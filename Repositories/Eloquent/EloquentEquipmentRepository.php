<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\EquipmentRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentEquipmentRepository extends EloquentBaseRepository implements EquipmentRepository
{
}
