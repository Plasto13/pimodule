<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\PlanRepairRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPlanRepairRepository extends EloquentBaseRepository implements PlanRepairRepository
{
    public function getCollumns()
    {
        return [
       'title' => [
            'name' => 'title',
            'data' => 'title',
            'title' => trans('pimodule::inspections.table.title'),
            'searchable' => true,
            'orderable' => true,
            // 'render' => 'function(){}',
            'footer' => trans('pimodule::inspections.table.title'),
            'exportable' => true,
            'printable' => true,
            'visible' => true,
        ],
        'description' => [
            'name' => 'description',
            'data' => 'description',
            'title' => trans('pimodule::inspections.table.description'),
            'searchable' => true,
            'orderable' => true,
            // 'render' => 'function(){}',
            'footer' => trans('pimodule::inspections.table.description'),
            'exportable' => true,
            'printable' => true,
            'visible' => true,
         ],
        'equipment_inspection_id' => [
            'name' => 'equipment_inspection_id',
            'data' => 'equipment_inspection_id',
            'title' => trans('pimodule::equipmentinspections.table.inspection'),
            'searchable' => true,
            'orderable' => true,
            // 'render' => 'function(){}',
            'footer' => trans('pimodule::equipmentinspections.table.inspection'),
            'exportable' => true,
            'printable' => true,
            'visible' => true,
        ],
        'equipmentInspection.equipment_id' => [
            'name' => 'equipment_inspection.equipment_id',
            'data' => 'equipment_inspection.equipment_id',
            'title' => trans('pimodule::equipmentinspections.table.equipment'),
            'searchable' => true,
            'orderable' => true,
            // 'render' => 'function(){}',
            'footer' => trans('pimodule::equipmentinspections.table.equipment'),
            'exportable' => true,
            'printable' => true,
            'visible' => true,
        ],       
        'user_id' => [
            'name' => 'user_id',
            'data' => 'user_id',
            'title' => trans('pimodule::planrepairs.table.user_id'),
            'searchable' => true,
            'orderable' => true,
            // 'render' => 'function(){}',
            'footer' => trans('pimodule::planrepairs.table.user_id'),
            'exportable' => true,
            'printable' => true,
            'visible' => true,
        ],
        'repair_date' => [
            'name' => 'repair_date',
            'data' => 'repair_date',
            'title' => trans('pimodule::planrepairs.table.repair_date'),
            'searchable' => true,
            'orderable' => true,
            // 'render' => 'function(){}',
            'footer' => trans('pimodule::planrepairs.table.repair_date'),
            'exportable' => true,
            'printable' => true,
            'visible' => true,
        ],
        'finished' => [
            'name' => 'ended',
            'data' => 'ended',
            'title' => trans('pimodule::planrepairs.table.finished'),
            'searchable' => true,
            'orderable' => true,
            // 'render' => 'function(){}',
            'footer' => trans('pimodule::planrepairs.table.finished'),
            'exportable' => true,
            'printable' => true,
            'visible' => true,
        ],
        'action' => [
            'defaultContent' => '',
            'data'           => 'action',
            'name'           => 'action',
            'title'          => trans('pimodule::planrepairs.table.action'),
            'render'         => null,
            'orderable'      => false,
            'searchable'     => false,
            'exportable'     => false,
            'printable'      => true,
            'footer'         => trans('pimodule::planrepairs.table.action'),
        ],

        ];
    }
}
