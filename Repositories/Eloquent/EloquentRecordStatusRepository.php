<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\RecordStatusRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentRecordStatusRepository extends EloquentBaseRepository implements RecordStatusRepository
{
}
