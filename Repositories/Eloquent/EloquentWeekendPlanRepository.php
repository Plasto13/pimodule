<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\WeekendPlanRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentWeekendPlanRepository extends EloquentBaseRepository implements WeekendPlanRepository
{
}
