<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\SettingsRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentSettingsRepository extends EloquentBaseRepository implements SettingsRepository
{
}
