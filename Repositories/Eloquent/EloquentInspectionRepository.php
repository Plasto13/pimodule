<?php

namespace Modules\Pimodule\Repositories\Eloquent;


use Modules\Pimodule\Events\InspectionWasCreated;
use Modules\Pimodule\Events\InspectionWasDeleted;
use Modules\Pimodule\Events\InspectionWasUpdated;
use Modules\Pimodule\Repositories\InspectionRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentInspectionRepository extends EloquentBaseRepository implements InspectionRepository
{

public function create($data)
    {
        $inspection = $this->model->create($data);
        event(new InspectionWasCreated($inspection, $data));
        return $inspection;
    }

public function update($inspection, $data)
{
    $inspection->update($data);
    event(new InspectionWasUpdated($inspection, $data));
    return $inspection;
}

public function destroy($inspection)
{
    event(new InspectionWasDeleted($inspection));
    return $inspection->delete();
}


	public function getCollumns()
	{
		$collumns = [
            'id' => [
                'name' => 'id',
                'data' => 'id',
                'title' => trans('pimodule::inspections.table.id'),
                'searchable' => false,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::inspections.table.id'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'title' => [
                'name' => 'title',
                'data' => 'title',
                'title' => trans('pimodule::inspections.table.title'),
                'searchable' => true,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::inspections.table.title'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'description' => [
                'name' => 'description',
                'data' => 'description',
                'title' => trans('pimodule::inspections.table.description'),
                'searchable' => true,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::inspections.table.description'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
			// 'equipment.cycle' => [
   //              'name' => 'cycle',
   //              'data' => 'equipment.cycle',
   //              'title' => trans('pimodule::inspections.table.cycle'),
   //              'searchable' => true,
   //              'orderable' => true,
   //              // 'render' => 'function(){}',
   //              'footer' => trans('pimodule::inspections.table.cycle'),
   //              'exportable' => true,
   //              'printable' => true,
   //              'visible' => true,
   //              ],
            'action' => [
                'defaultContent' => '',
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('pimodule::inspections.table.action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => trans('pimodule::inspections.table.action'),
                ],
            ];
        return $collumns;
	}

   public function getTable($data,$relationships,$user,$id = null){
        $html = [];

        foreach($data as $i){
            if ($id) {
                $detail = $i->$relationships()->where($relationships.'_id',$id)->first();
            }
            
            $html[] = "<tr><td>".\Form::checkbox('equipment['.$i->id.'][equipment_id]',$i->id,isset($detail),['class' => 'flat-blue'])."</td><td>".$i->line->hall->equipment_name.' - '.$i->line->equipment_name.' - '.$i->equipment_name."</td>
            <td>".\Form::select(
                            'equipment['.$i->id.'][user_id]',
                            $user,isset($detail['pivot']['user_id']) ? $detail['pivot']['user_id'] : null,
                            ['placeholder'=>'Select User','class'=>'form-control']).
                "</td>
                <td>"
                    .\Form::number('equipment['.$i->id.'][cycle]',isset($detail['pivot']['cycle']) ? $detail['pivot']['cycle'] : 7,['class'=>'form-control']).
                "</td>
            </tr>";                    
        }
        return $html;
    }
}
