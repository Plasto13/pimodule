<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\RecordRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;
use Modules\Pimodule\Events\RecordWasCreated;
use Modules\Pimodule\Events\RecordWasUpdated;
use Modules\Pimodule\Events\RecordWasDeleted;

class EloquentRecordRepository extends EloquentBaseRepository implements RecordRepository
{
    protected $model;

    public function create($data)
    {
        $item = $this->model->create($data);

        event(new RecordWasCreated($item, $data));

        return $item;
    }


    public function update($item, $data)
    {
        $item->update($data);

        event(new RecordWasUpdated($item, $data));

        return $item;
    }

    public function destroy($item)
    {
        event(new RecordWasDeleted($item));

        return $item->delete();
    }

	public function getCollumns()
	{
		return $collumns = [
			'id' => [
                'name' => 'id',
                'data' => 'id',
                'title' => trans('pimodule::records.table.id'),
                'searchable' => false,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::records.table.id'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'equipment' => [
                'name' => 'equipment',
                'data' => 'equipment',
                'title' => trans('pimodule::records.table.equipment'),
                'searchable' => true,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::records.table.equipment'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'inspection' => [
                'name' => 'inspection',
                'data' => 'inspection',
                'title' => trans('pimodule::records.table.inspection'),
                'searchable' => true,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::records.table.inspection'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            // 'equipment_inspection_id' => [
            //     'name' => 'equipment_inspection_id',
            //     'data' => 'equipment_inspection_id',
            //     'title' => trans('pimodule::records.table.equipment_inspection_id'),
            //     'searchable' => true,
            //     'orderable' => true,
            //     // 'render' => 'function(){}',
            //     'footer' => trans('pimodule::records.table.equipment_inspection_id'),
            //     'exportable' => true,
            //     'printable' => true,
            //     'visible' => true,
            //     ],
            'plan_repair' => [
                'name' => 'plan_repair',
                'data' => 'plan_repair',
                'title' => trans('pimodule::records.table.plan_repair'),
                'searchable' => true,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::records.table.plan_repair'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'record_status_id' => [
                'name' => 'record_status_id',
                'data' => 'record_status_id',
                'title' => trans('pimodule::records.table.record_status_id'),
                'searchable' => true,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::records.table.record_status_id'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'description' => [
                'name' => 'description',
                'data' => 'description',
                'title' => trans('pimodule::records.table.description'),
                'searchable' => true,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::records.table.description'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'user.last_name' => [
                'name' => 'user.last_name',
                'data' => 'user.last_name',
                'title' => trans('pimodule::records.table.inspector'),
                'searchable' => true,
                'orderable' => true,
                // 'render' => 'function(){}',
                'footer' => trans('pimodule::records.table.inspector'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'photo' => [
                'name' => 'photo',
                'data' => 'photo',
                'title' => trans('warehouse::items.table.photo'),
                'searchable' => false,
                'orderable' => false,
                'footer' => trans('warehouse::items.table.photo'),
                'exportable' => true,
                'printable' => true,
                ],
            'created_at' => [
                'name' => 'created_at',
                'data' => 'created_at',
                'title' => trans('pimodule::records.table.created_at'),
                'searchable' => true,
                'orderable' => true,
                'footer' => trans('pimodule::records.table.created_at'),
                'exportable' => true,
                'printable' => true,
                'visible' => true,
                ],
            'action' => [
                'defaultContent' => '',
                'data'           => 'action',
                'name'           => 'action',
                'title'          => trans('pimodule::records.table.action'),
                'render'         => null,
                'orderable'      => false,
                'searchable'     => false,
                'exportable'     => false,
                'printable'      => true,
                'footer'         => trans('pimodule::records.table.action'),
                ],
		];
	}
}
