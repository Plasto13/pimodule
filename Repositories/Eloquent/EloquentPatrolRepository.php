<?php

namespace Modules\Pimodule\Repositories\Eloquent;

use Modules\Pimodule\Repositories\PatrolRepository;
use Modules\Core\Repositories\Eloquent\EloquentBaseRepository;

class EloquentPatrolRepository extends EloquentBaseRepository implements PatrolRepository
{
}
