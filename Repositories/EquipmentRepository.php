<?php

namespace Modules\Pimodule\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface EquipmentRepository extends BaseRepository
{
}
