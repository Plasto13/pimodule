<?php

namespace Modules\Pimodule\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface RecordStatusRepository extends BaseRepository
{
}
