<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\EquipmentInspectionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheEquipmentInspectionDecorator extends BaseCacheDecorator implements EquipmentInspectionRepository
{
    public function __construct(EquipmentInspectionRepository $equipmentinspection)
    {
        parent::__construct();
        $this->entityName = 'pimodule.equipmentinspections';
        $this->repository = $equipmentinspection;
    }
}
