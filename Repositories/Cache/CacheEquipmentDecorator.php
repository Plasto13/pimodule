<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\EquipmentRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheEquipmentDecorator extends BaseCacheDecorator implements EquipmentRepository
{
    public function __construct(EquipmentRepository $equipment)
    {
        parent::__construct();
        $this->entityName = 'pimodule.equipment';
        $this->repository = $equipment;
    }
}
