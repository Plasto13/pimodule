<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\PreventiveInspectionCheckRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePreventiveInspectionCheckDecorator extends BaseCacheDecorator implements PreventiveInspectionCheckRepository
{
    public function __construct(PreventiveInspectionCheckRepository $preventiveinspectioncheck)
    {
        parent::__construct();
        $this->entityName = 'pimodule.preventiveinspectionchecks';
        $this->repository = $preventiveinspectioncheck;
    }
}
