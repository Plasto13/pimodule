<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\SettingsRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheSettingsDecorator extends BaseCacheDecorator implements SettingsRepository
{
    public function __construct(SettingsRepository $settings)
    {
        parent::__construct();
        $this->entityName = 'pimodule.settings';
        $this->repository = $settings;
    }
}
