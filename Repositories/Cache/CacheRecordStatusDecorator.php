<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\RecordStatusRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRecordStatusDecorator extends BaseCacheDecorator implements RecordStatusRepository
{
    public function __construct(RecordStatusRepository $recordstatus)
    {
        parent::__construct();
        $this->entityName = 'pimodule.recordstatuses';
        $this->repository = $recordstatus;
    }
}
