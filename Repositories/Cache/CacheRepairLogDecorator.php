<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\RepairLogRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRepairLogDecorator extends BaseCacheDecorator implements RepairLogRepository
{
    public function __construct(RepairLogRepository $repairlog)
    {
        parent::__construct();
        $this->entityName = 'pimodule.repairlogs';
        $this->repository = $repairlog;
    }
}
