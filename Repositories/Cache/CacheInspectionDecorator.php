<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\InspectionRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheInspectionDecorator extends BaseCacheDecorator implements InspectionRepository
{
    public function __construct(InspectionRepository $inspection)
    {
        parent::__construct();
        $this->entityName = 'pimodule.inspections';
        $this->repository = $inspection;
    }
}
