<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\PatrolRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePatrolDecorator extends BaseCacheDecorator implements PatrolRepository
{
    public function __construct(PatrolRepository $patrol)
    {
        parent::__construct();
        $this->entityName = 'pimodule.patrols';
        $this->repository = $patrol;
    }
}
