<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\RecordRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheRecordDecorator extends BaseCacheDecorator implements RecordRepository
{
    public function __construct(RecordRepository $record)
    {
        parent::__construct();
        $this->entityName = 'pimodule.records';
        $this->repository = $record;
    }
}
