<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\WeekendPlanRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CacheWeekendPlanDecorator extends BaseCacheDecorator implements WeekendPlanRepository
{
    public function __construct(WeekendPlanRepository $weekendplan)
    {
        parent::__construct();
        $this->entityName = 'pimodule.weekendplans';
        $this->repository = $weekendplan;
    }
}
