<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\PlanRepairRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePlanRepairDecorator extends BaseCacheDecorator implements PlanRepairRepository
{
    public function __construct(PlanRepairRepository $planrepair)
    {
        parent::__construct();
        $this->entityName = 'pimodule.planrepairs';
        $this->repository = $planrepair;
    }
}
