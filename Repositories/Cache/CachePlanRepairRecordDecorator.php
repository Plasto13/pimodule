<?php

namespace Modules\Pimodule\Repositories\Cache;

use Modules\Pimodule\Repositories\PlanRepairRecordRepository;
use Modules\Core\Repositories\Cache\BaseCacheDecorator;

class CachePlanRepairRecordDecorator extends BaseCacheDecorator implements PlanRepairRecordRepository
{
    public function __construct(PlanRepairRecordRepository $planrepairrecord)
    {
        parent::__construct();
        $this->entityName = 'pimodule.planrepairrecords';
        $this->repository = $planrepairrecord;
    }
}
