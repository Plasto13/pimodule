<?php

namespace Modules\Pimodule\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface InspectionRepository extends BaseRepository
{
}
