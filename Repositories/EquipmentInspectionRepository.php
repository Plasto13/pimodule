<?php

namespace Modules\Pimodule\Repositories;

use Modules\Core\Repositories\BaseRepository;

interface EquipmentInspectionRepository extends BaseRepository
{
}
