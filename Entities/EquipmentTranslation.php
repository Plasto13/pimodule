<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class EquipmentTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__equipment_translations';
}
