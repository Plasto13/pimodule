<?php

namespace Modules\Pimodule\Entities;

// use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class WeekendPlan extends Model
{
    // use Translatable;

    protected $table = 'pimodule__weekendplans';
    // public $translatedAttributes = [];
    protected $fillable = ['title', 'description', 'remarks' , 'start_date', 'end_date', 'users', 'estimated_time', 'extra','hall_id','line_id','equipment_id'];
    protected $cast = ['extra' => 'array', 'users' => 'array'];
    protected $dates = [ 'start_date', 'end_date'];
}
