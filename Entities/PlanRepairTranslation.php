<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class PlanRepairTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__planrepair_translations';
}
