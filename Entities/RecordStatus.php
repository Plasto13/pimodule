<?php

namespace Modules\Pimodule\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class RecordStatus extends Model
{
    // use Translatable;

    protected $table = 'pimodule__recordstatuses';
    public $translatedAttributes = [];
    protected $fillable = ['title','description'];

    public function record()    
    {
    	return $this->hasMany(Record::class, 'record_status_id');
    }
}
