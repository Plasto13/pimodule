<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class WeekendPlanTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__weekendplan_translations';
}
