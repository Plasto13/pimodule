<?php

namespace Modules\Pimodule\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    // use Translatable;

    protected $table = 'pimodule__settings';
    // public $translatedAttributes = [];
    protected $fillable = ['prewarning_days','prewarning_color','warning_color','repair_color','extra'];


}
