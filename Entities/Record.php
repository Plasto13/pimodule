<?php

namespace Modules\Pimodule\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Pimodule\Entities\EquipmentInspection;
use Modules\Pimodule\Entities\PlanRepair;
use Modules\Pimodule\Entities\RecordStatus;
use Modules\Base\Entities\Equipment;
use Modules\Pimodule\Entities\Inspection;
use Modules\User\Entities\Sentinel\User;
use Modules\Media\Support\Traits\MediaRelation;

class Record extends Model
{
    use MediaRelation;
    // use Translatable;

    protected $table = 'pimodule__records';
    public $translatedAttributes = [];
    protected $fillable = ['status', 'description', 'plan_repair', 'inspector', 'record_status_id', 'equipment', 'inspection', 'equipment_id', 'inspection_id', 'user_id', 'planed'];
    protected $casts = [
        'plan_repair' => 'boolean',
        'planed' => 'boolean',
    ];

    public function equipmentInspection()
    {
        return $this->belongsTo(EquipmentInspection::class);
    }

    public function status()
    {
        return $this->belongsTo(RecordStatus::class, 'record_status_id');
    }

    public function equipment()
    {
        return $this->belongsTo(Equipment::class);
    }

    public function inspection()
    {
        return $this->belongsTo(Inspection::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function planRepair()
    {
        return $this->hasOne(PlanRepair::class);
    }

    /**
     * Relationship to result pictures
     */
    public function getResultPictureAttribute()
    {
        return $this->filesByZone('record_result');
    }

}
