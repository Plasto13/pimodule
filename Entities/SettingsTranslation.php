<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class SettingsTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__settings_translations';
}
