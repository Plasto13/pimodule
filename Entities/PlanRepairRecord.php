<?php

namespace Modules\Pimodule\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class PlanRepairRecord extends Model
{
    use Translatable;

    protected $table = 'pimodule__planrepairrecords';
    public $translatedAttributes = [];
    protected $fillable = [];
}
