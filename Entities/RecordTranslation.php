<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class RecordTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__record_translations';
}
