<?php

namespace Modules\Pimodule\Entities;

// use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Pimodule\Entities\EquipmentInspection;
use Modules\Pimodule\Entities\Record;
use Modules\Pimodule\Entities\RepairLog;
use Modules\User\Entities\Sentinel\User;

class PlanRepair extends Model
{
    // use Translatable;

    protected $table = 'pimodule__planrepairs';
    // public $translatedAttributes = [];
    protected $fillable = ['user_id', 'record_id', 'repair_date', 'title', 'description', 'finished', 'extra'];

    protected $cast = ['extra' => 'array'];

    protected $dates = ['repair_date'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

	public function record()
	{
		return $this->belongsTo(Record::class);
	}

	public function equipmentInspection()
	{
		return $this->belongsTo(EquipmentInspection::class);
	}

	public function repairLog()
	{
		return $this->hasMany(RepairLog::class);
	}

	    public function getRepairPictureAttribute()
    {
        return $this->filesByZone('planrepair')->first();
    }
}
