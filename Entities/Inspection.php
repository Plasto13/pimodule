<?php

namespace Modules\Pimodule\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Modules\Base\Entities\Equipment;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;
use Modules\Pimodule\Entities\EquipmentInspection;
use Modules\Pimodule\Entities\Record;
use Modules\Media\Support\Traits\MediaRelation;

class Inspection extends Model
{
    // use Translatable;
    use MediaRelation;

    protected $table = 'pimodule__inspections';
    // public $translatedAttributes = [];
    protected $fillable = ['title','description','cycle'];
    // protected $dates = ['last_check'];

    public function getInstructionAttribute()
    {
        return $this->filesByZone('instruction')->first();
    }

    public function equipment()
    {
    	return  $this->belongsToMany(Equipment::class,'pimodule__equipment_inspection')
                    ->using(EquipmentInspection::class)
    				->withPivot('last_check','next_check','user_name','cycle','user_id')
                    ->withTimestamps();
    }

    public function hall()
    {
        return  $this->belongsToMany(Hall::class,'pimodule__equipment_inspection')
                    ->using(EquipmentInspection::class)
                    ->withPivot('last_check','next_check','user_name','cycle','user_id')
                    ->withTimestamps();
    }
    public function line()
    {
        return  $this->belongsToMany(Line::class,'pimodule__equipment_inspection')
                    ->using(EquipmentInspection::class)
                    ->withPivot('last_check','next_check','user_name','cycle','user_id')
                    ->withTimestamps();
    }

    public function record()
    {
        return $this->hasManny(Record::class);
    }
    
}
