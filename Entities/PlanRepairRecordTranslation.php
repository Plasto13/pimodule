<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class PlanRepairRecordTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__planrepairrecord_translations';
}
