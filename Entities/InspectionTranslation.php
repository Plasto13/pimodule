<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class InspectionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = ['title','description'];
    protected $table = 'pimodule__inspection_translations';
}
