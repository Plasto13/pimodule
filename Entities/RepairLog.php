<?php

namespace Modules\Pimodule\Entities;

// use Dimsav\Translatable\Translatable;
use Modules\User\Entities\Sentinel\User;
use Modules\Pimodule\Entities\PlanRepair;
use Illuminate\Database\Eloquent\Model;

class RepairLog extends Model
{
    // use Translatable;

    protected $table = 'pimodule__repairlogs';
    // public $translatedAttributes = [];
    protected $fillable = ['title', 'description', 'users', 'ended'];

    protected $casts = ['ended' => 'boolean', 'users' => 'array'];

    public function planRepair()
    {
    	return $this->belongsTo(PlanRepair::class);
    }
}
