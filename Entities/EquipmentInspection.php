<?php

namespace Modules\Pimodule\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Modules\Media\Support\Traits\MediaRelation;
use Modules\Base\Entities\Equipment;
use Modules\Base\Entities\Hall;
use Modules\Base\Entities\Line;
use Modules\Pimodule\Entities\Inspection;
use Modules\Pimodule\Entities\PlanRepair;
use Modules\Pimodule\Entities\Record;
use Modules\User\Entities\Sentinel\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class EquipmentInspection extends Pivot
{
    // use Translatable;
    use MediaRelation;
    use SoftDeletes;

    protected $table = 'pimodule__equipment_inspection';
    // public $translatedAttributes = [];
    protected $fillable = [
                        'user_id',
                        'inspection_id',
                        'hall_id',
                        'line_id',
                        'equipment_id',
    					'last_check',
    					'next_check',
                        'user_name',
                        'cycle',
                        'duration'
    					];
    protected $cast = [
    				// 'last_check' => 'datetime',
    				// 'next_check' => 'datetime',
    				];

	protected $dates = [
					'last_check',
					'next_check',
					'deleted_at',
					];

    
    /**
     * Relationship to instruction
     */
    public function getInstructionAttribute()
    {
        return $this->filesByZone('preventive_instruction')->first();
    }

    public function hall()
    {
        return $this->belongsTo(Hall::class);
    } 
    public function line()
    {
        return $this->belongsTo(Line::class);
    }

    public function equipment()
    {
    	return $this->belongsTo(Equipment::class);
    }

    public function inspection()
    {
    	return $this->belongsTo(Inspection::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function record()
    {
        return $this->hasMany(Record::class,'equipment_inspection_id');
    }

    public function planRepair()
    {
        return $this->hasMany(PlanRepair::class, 'equipment_inspection_id');
    }

    /**
     * MUTATORS
     */
    
    public function getCreatedAtColumn()
    {
        return 'created_at';
    }

    public function getUpdatedAtColumn()
    {
        return 'updated_at';
    }
}
