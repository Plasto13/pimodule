<?php

namespace Modules\Pimodule\Entities;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class PreventiveInspectionCheck extends Model
{
    use Translatable;

    protected $table = 'pimodule__preventiveinspectionchecks';
    public $translatedAttributes = [];
    protected $fillable = [];
}
