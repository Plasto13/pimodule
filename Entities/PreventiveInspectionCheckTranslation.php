<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class PreventiveInspectionCheckTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__preventiveinspectioncheck_translations';
}
