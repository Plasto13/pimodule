<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class EquipmentInspectionTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__equipmentinspection_translations';
}
