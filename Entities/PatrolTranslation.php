<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class PatrolTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__patrol_translations';
}
