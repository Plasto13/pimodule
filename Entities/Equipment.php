<?php

namespace Modules\Pimodule\Entities;

// use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Equipment extends Model
{
    // use Translatable;

    protected $table = 'pimodule__equipment';
    // public $translatedAttributes = [];
    protected $fillable = [];
}
