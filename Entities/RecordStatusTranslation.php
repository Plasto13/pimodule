<?php

namespace Modules\Pimodule\Entities;

use Illuminate\Database\Eloquent\Model;

class RecordStatusTranslation extends Model
{
    public $timestamps = false;
    protected $fillable = [];
    protected $table = 'pimodule__recordstatus_translations';
}
