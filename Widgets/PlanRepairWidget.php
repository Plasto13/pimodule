<?php

namespace Modules\Pimodule\Widgets;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Modules\Dashboard\Foundation\Widgets\BaseWidget;
use Modules\Pimodule\Entities\PlanRepair;

class PlanRepairWidget extends BaseWidget
{
    /**
     * @var \Modules\Blog\Repositories\PlanRepairRepository
     */
    private $planRepair;

    public function __construct(PlanRepair $planRepair)
    {
        $this->planRepair = $planRepair;
    }

    /**
     * Get the widget name
     * @return string
     */
    protected function name()
    {
        return 'PlanRepairWidget';
    }

    /**
     * Get the widget view
     * @return string
     */
    protected function view()
    {
        return 'pimodule::admin.widgets.planrepair';
    }

    /**
     * Get the widget data to send to the view
     * @return string
     */
    protected function data()
    {
        $data = $this->planRepair->where('finished', '=', null)->get();


        return ['count' => $data->count(),
                'groupByDate' => $data->sortBy('repair_date')
                                        ->groupBy(function($item) {
                                            return $item->repair_date->format('d.m.Y');
                                        }),
        ];
    }

    /**
    * Get the widget type
    * @return string
    */
    protected function options()
    {
        return [
            'width' => '3',
            'height' => '2',
            'x' => '0',
            'access' => 'pristup'
        ];
    }
}