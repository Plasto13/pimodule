<?php

namespace Modules\Pimodule\Widgets;

use Carbon\Carbon;
use Modules\Dashboard\Foundation\Widgets\BaseWidget;
use Modules\Pimodule\Repositories\EquipmentInspectionRepository;
use Modules\User\Contracts\Authentication;

class EquipmentInspectionWidget extends BaseWidget
{
    /**
    * @var Authentication
    */
    private $auth;

    /**
     * @var \Modules\Blog\Repositories\EquipmentInspectionRepository
     */
    private $equipmentInspeciton;

    public function __construct(EquipmentInspectionRepository $equipmentInspeciton, Authentication $auth)
    {
        $this->equipmentInspeciton = $equipmentInspeciton;
        $this->auth = $auth;
    }

    /**
     * Get the widget name
     * @return string
     */
    protected function name()
    {
             return 'EquipmentInspectionWidget';
       
    }

    /**
     * Get the widget view
     * @return string
     */
    protected function view()
    {
            return 'pimodule::admin.widgets.equipmentinspeciton';
    }

    /**
     * Get the widget data to send to the view
     * @return string
     */
    protected function data()
    {
        $data = $this->equipmentInspeciton->all();
        return ['equipmentInspeciton' => $data->count(), 
                'auth' => $this->auth,
                'inspectionProgress' => $data->where('next_check', '>', Carbon::now())->count(),
                'inspectionMissing' => $data->where('next_check', '<', Carbon::now())->count(),
        ];
    }

    /**
    * Get the widget type
    * @return string
    */
    protected function options()
    {
        return [
            'width' => '3',
            'height' => '2',
            'x' => '0',
        ];
    }
}